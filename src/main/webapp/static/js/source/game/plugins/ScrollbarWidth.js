;(function() {

	define(
		"plugins/ScrollbarWidth",
		[
			//
		],
		function () {

			window.scrollbarWidth = function() {
				var parent, child, width;

				if(width===undefined) {

					parent = $('<div/>')
						.css({"width" : "50px", "height" : "50px", "overflow" : "auto"})
						.append($('<div/>'))
						.appendTo(App.siteElement);

					child=parent.children();
					width=child.innerWidth()-child.height(99).innerWidth();
					parent.remove();
				}

				return width;
			};

			return window.scrollbarWidth;
		}
	);
})($);