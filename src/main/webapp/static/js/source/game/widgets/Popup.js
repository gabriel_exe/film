;(function($) {

	define(
		"widgets/Popup",
		[
			"plugins/ScrollbarWidth"
		],
		function (ScrollbarWidth) {

			// TODO
			// Написать модал-менеджер, для управления модалками ВНЕ самих модалок.
			// Сейчас, получается что модалка вызывает/скрывает сама себя.
			// Этот код тоже нужно переработать.

			(function ($) {
				var totalpop = 0,
					progress = false;

				window.Popup = function ($this, options) {
					var _this = this, oldClass = $this.attr('class');
					totalpop++;

					var opts = $.extend({
							"closeButton" : true,
							modalOverlayClose : true,
							modalClone: false,
							"duration": 200,
							"scrolledArea" : App.siteElement,
							"cssClass" : ""
						}, $this.data(), options),
						visible = $this.is(":visible"),
						currentpop = totalpop;


					_this.overlay = $('<div/>', {"class" : "modal-overlay"});
					_this.temporary = $('<div class="dummy">');
					_this.wrapper = $('<div/>', {"class" : "modal-wrapper"});






					$this[0].popup = _this;

					_this.showModal = function () {

						_this.disablePageScroll();

						if (opts.cssClass) _this.wrapper.find(".modal").addClass(opts.cssClass);



						if (_this.wrapper.is(":visible")) {
							_this.updatePosition();
							return;
						}


						visible || $this.css({"display" : "block"});

						if (opts.modalClone) {
							$this = $this.clone(true);
						} else {
							$this = $this.before(_this.temporary).detach();
						}

						//Make it visible to define sizes
						_this.wrapper.stop().css("z-index", 1002 + currentpop * 3);
						_this.overlay.stop().css({
							"z-index": 1001 + currentpop * 3
						});

						_this.wrapper.append($this);

						if (opts.closeButton) _this.addCloseButton();

						$('.site-wrapper')
							.append(_this.wrapper.css({"display" : "block"}))
							.append(_this.overlay);

						$(window)
							.bind('resize', _this.updatePosition)
							.bind('keydown', _this.observeEscapePress);

						_this.wrapper.bind('resize', _this.updatePosition);



						_this.updatePosition();


						_this.overlay.fadeIn(opts.duration, function () {
							if(opts.modalOverlayClose){
								_this.wrapper.on('click', function(e){
									if(!$(e.target).closest(".modal").length > 0){
										_this.removeModal();
									}
								});
							}
						});
						_this.wrapper.css('opacity', 0).show().animate({'opacity': 1}, opts.duration, function () {
							$this.triggerHandler('popupShow');
						});


					};

					_this.removeModal = function () {

						_this.overlay.stop().fadeOut(300, function () {
							_this.overlay.remove();
						});
						_this.wrapper.stop().fadeOut(300, function () {

							$(window)
								.unbind('scroll resize', _this.updatePosition)
								.unbind('keydown', _this.observeEscapePress);
							_this.wrapper.unbind('resize', _this.updatePosition);

							$('.modal-close', _this.wrapper).unbind('click', _this.removeModal);
							_this.overlay.unbind('click', _this.removeModal);


							_this.temporary.after($this.detach()).remove();

							_this.wrapper.find(".modal-close").remove();
							_this.wrapper.remove();
							visible || $this.hide();


							$this.trigger('event-modal-close');

							_this.enablePageScroll();

						});

					};

					_this.observeEscapePress = function (e) {
						if (e.keyCode == 27 || (e.DOM_VK_ESCAPE == 27 && e.which == 0)) {
							if (currentpop >= totalpop) {
								_this.removeModal();
							}
						}
					};

					_this.updatePosition = function () {

						var windowWidth = $(window).width(),
							windowHeight = $(window).height(),
							modalWidth = _this.wrapper.find(".modal").outerWidth(true),
							modalHeight = _this.wrapper.find(".modal").outerHeight(true),
							modalLeftPosition = (windowWidth - modalWidth) / 2,
							modalTopPosition = (modalHeight < windowHeight)
								? (windowHeight - modalHeight) / 2
								: "0px";



						_this.wrapper.find(".modal")
							.css({
								"left" : modalLeftPosition,
								"top" : modalTopPosition
							});

						return _this;
					};

					_this.disablePageScroll = function () {

						var scrollbarWidth = window.scrollbarWidth(),
							cssProperties = {
								"marginLeft" : (scrollbarWidth/2) * -1,
								"overflow" : "hidden"
							};

						opts.scrolledArea.css(cssProperties);
					};

					_this.enablePageScroll = function () {

						var scrollbarWidth = $.fn.scrollbarWidth(),
							cssProperties = {
								"marginLeft" : 0,
								"overflow" : "auto"
							};

						opts.scrolledArea.css(cssProperties);
					};

					_this.addCloseButton = function() {
						var button = $('<i>', {"class": "modal-close user-select", "title": "Закрыть"})
							.append($("<span>", {}))
							.click( _this.removeModal);

						_this.wrapper.find(".modal").append(button);

					};

					return _this;
				};

				$.fn.showPopup = function (options) {
					if (this.length == 0)
						return this;
					if (!this[0].popup) {
						new Popup(this, options);
					}

					this[0].popup.showModal();

					return this;
				};

				$.fn.hidePopup = function (options) {
					if (this.length == 0)
						return this;

					if (!this[0].popup)
						new Popup(this, options);

					this[0].popup.removeModal();
					return this;
				}
			})($);
		}
	)
})($);