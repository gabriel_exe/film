(function($){

	define(
		"widgets/Tabs",
		[
			//
		],
		function(){

			function Tabs(options){
				this.init(options);
			}

			Tabs.prototype = {
				"constructor" : Tabs,
				"init" : function(options){

					var defaults = {
							"element" : null
						},
						namespace = this;

					$.extend(true, this, defaults, options);

					this.tabsHeadersElements = !this.tabsHeadersElements
						? $(".tabs-headers-list li", this.element)
						: this.tabsHeadersElements;

					this.tabsContentElements = !this.tabsContentElements
						? $(".tabs-content-list li", this.element)
						: this.tabsContentElements;

					if(!this.element){
						return false;
					}

					this.tabsHeadersElements.each(function(){
						$(this).on("click", function(){
							namespace.showTab($(this).data("tab-id"));
						})
					});

					if(this.element.closest(".ajax-zone")){
						this.element.closest(".ajax-zone").on("zone-update", function(){
							namespace.init(options);
						});
					}

					this.element.addClass("tabs-initialized");

				},

				"showTab" : function(tabId){

					this.tabsHeadersElements
						.filter(".active")
						.removeClass("active")
						.end()
						.filter("[data-tab-id=" + tabId + "]")
						.addClass("active");

					this.tabsContentElements
						.filter(".active")
						.removeClass("active")
						.end()
						.filter("[data-tab-id=" + tabId + "]")
						.addClass("active");

					return this;
				},

				"hideTab" : function(tabId){
					this.tabsHeadersElements
						.filter("[data-tab-id=" + tabId + "]")
						.removeClass("active");

					this.tabsContentElements
						.filter("[data-tab-id=" + tabId + "]")
						.removeClass("active");

					return this;
				}
			};

			return Tabs;

		}
	);

})(jQuery);