(function($){
    define(
        "widgets/battle/BattleAction",
        [
			"widgets/battle/Card"
		],
        function(Card){

            function BattleAction(options){
                this.init(options);
            }

            BattleAction.prototype = {

                "constructor" : "BattleAction",

                "init" : function(){

                    this.cardNumber = null;
					this.literal = null;
                    this.target = null;
					this.actionType = null;
					this.heroActionDelay = 0;
					this.armyAttackDelay = 0;

                    var namespace = this;




					$("#travel-pass").on("click", function(){
						if(!$(this).hasClass("disabled")){
							$(".deck").addClass("disabled");
							Game.MyArmy.deactivateSlots();
							Game.MyDeck.deactivateAllCards();
							Game.MyDeck.removeEvents();
							namespace.actionType = "travelPass";
							namespace.cardNumber = null;
							namespace.target = 100;
							namespace.send();
						}
					});

					$("#surrender").on("click", function(){
						if(!$(this).hasClass("disabled")){
							$(".deck").addClass("disabled");
							Game.MyArmy.deactivateSlots();
							Game.MyDeck.deactivateAllCards();
							Game.MyDeck.removeEvents();
							namespace.actionType = "surrender";
							namespace.cardNumber = null;
							namespace.target = 100;
							namespace.send();
						}
					});

					if($("#currentActionPointer").length <= 0){
						namespace.trackActionOpponent();
					}


                },

                "send" : function(){

                    var namespace = this;

					new Ajax.Request('/battlefield:heroAction', {
						method: 'POST',
						parameters: {
							"heroId" : $(".hero.my").data("hero-id"),
							"actionType": namespace.actionType,
							"cardNumber": namespace.cardNumber,
							"literal" : namespace.literal,
							"target": namespace.target
						},
						onSuccess: function (transport) {

							var data = transport.responseText,
								json = $.parseJSON(data),
								status = json["status"];

							if(status == "success"){
								namespace.displayResult(json);
								namespace.actionType = null;
								namespace.cardNumber = null;
								namespace.target = null;
								namespace.literal = null;
							} else {
								namespace.displayError(json);
							}
						}
					});

					Game.MyDeck.deactivateAllCards();
					$("#travel-pass").addClass("disabled");
					$("#surrender").addClass("disabled");
                },

                "displayResult" : function(json){

					console.log(json);

					var namespace = this,
						heroAction = json["heroAction"][0],
						heroActionResult = heroAction["actionResult"][0],
						heroId = heroAction["heroId"][0],
						actionType = heroAction["actionType"][0];


					switch (actionType) {
						case "setCard" : Game[heroId].setCard(heroAction); break;
						case "castSpell" : Game[heroId].castSpell(heroAction); break;
						case "surrender" : Game[heroId].surrender(heroAction); break;
					}

					if(actionType != "surrender"){
						if(heroActionResult == "continues"){
							var cards = json["armyAttack"][0]["cards"][0],
								armyAttackResult = json["armyAttack"][0]["armyAttackResult"][0];
							for(var i in cards){
								if(cards.hasOwnProperty(i)){
									var cardObj = cards[i][0];
									for(var cardNumber in cardObj){
										if(cardObj.hasOwnProperty(cardNumber)){
											setTimeout(function(cardNumber, obj){
												Game.cards[cardNumber].attack(obj);
											}, namespace.heroActionDelay + (namespace.armyAttackDelay+=2000), cardNumber, cardObj[cardNumber]);
										}
									}
								}
							}



							if(armyAttackResult == "continues"){

								var growthPowers = json["growthPowers"][0],
									powersId = growthPowers["powersId"][0],
									powers = growthPowers["powers"][0],
									preAction = json["preAction"][0];

								setTimeout(function(){

									Game["powers_" + powersId].changePowers(powers, false);
									namespace.preAction(preAction);
									namespace.heroActionDelay = 0;
									namespace.armyAttackDelay = 0;

									if($(".hero.my").attr("id") == heroId){
										//Если отображается мой ход, запускаем отслеживание хода соперника
										namespace.trackActionOpponent();
									} else {
										//Если отображается ход соперника, разблокируем мою колоду
										Game.MyDeck.updateAvailableToActionCards(json["availableToActionCards"][0]);
										$("#travel-pass").removeClass("disabled");
										$("#surrender").removeClass("disabled");
									}
								}, namespace.heroActionDelay + (namespace.armyAttackDelay) + 2000);

							} else {

								var winner = armyAttackResult["winner"][0],
									looser = armyAttackResult["looser"][0],
									text = winner + " одерживает победу, " + looser + " погиб в бою.\r\n" +
										"Для того чтобы начать новую битву, заново пригласите соперника";
								setTimeout(function(){
									alert(text);
								}, namespace.heroActionDelay + (namespace.armyAttackDelay) + 2000);

							}
						}
					}
                },

				"trackActionOpponent" : function(){

					var namespace = this;

					var trackFn = function(){


						new Ajax.Request("/battlefield:checkActionOpponent",{
							"method" : "POST",
							"parameters" : {
								"heroId" : $(".hero.my").data("hero-id")
							},
							"onSuccess" : function(transport){

								var data = transport.responseText,
									json = $.parseJSON(data),
									status = json["status"];

								if(status == "error"){
									//namespace.displayError(json);
								}
								else if (status == "done"){
									namespace.displayResult($.parseJSON(json["actionResult"]));
									clearInterval(namespace.timer_trackActionOpponent);
								}
							}
						})
					};

					namespace.timer_trackActionOpponent = setInterval(trackFn, 1000);
				},

				"displayError" : function(json){
					var errorType = json["errorType"],
						errorMessage = json["errorMessage"],
						text = "Приложение ответило ошибкой. " +
							"\r\nТип ошибки: " + errorType +
							"\r\nСообщение: " + errorMessage +
							"\r\nЗакройте данное сообшение, обновите страницу и повторите действие";

					alert(text);


				},

				"preAction" : function(preAction){

					var namespace = this,
						delay = 0;

					for(var i in preAction){
						if(preAction.hasOwnProperty(i)){


							var obj = preAction[i][0];
							setTimeout(function(obj){

								var cardNumber = obj["accentCard"][0],
									targets = obj["targets"][0];

								Game.cards[cardNumber].accent();

								for(var target in targets){
									if(targets.hasOwnProperty(target)){
										var o = targets[target][0],
											t = o["target"][0],
											count = o["count"][0],
											newHP = o["newHP"][0],
											result = o["result"][0];

										switch (t.substr(0,4)){
											 case "card" : Game.cards[t].displayChangeHP(count, newHP, result); break;
											 case "hero" : Game[t].displayChangeHP(count, newHP); break;
										}
									}
								}
							}, delay+=1000, obj)

						}
					}
				}
            };

            return BattleAction;

        }
    )
})(jQuery);
