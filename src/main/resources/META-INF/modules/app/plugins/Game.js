(function($){
    define(
        "app/plugins/Game",
        [
            "app/plugins/jquery/swiper",
            "app/plugins/jquery/jquery.countdown"
        ],
        function(Swiper){

            function Game(options){
                this.init(options)
            }

            Game.prototype = {
                "constructor" : "Game",
                "init" : function(options){

                    var _this = this;

                    this.element = $("#gameZone");

                    this.initTimer();
                    this.initImageToggle();
                    this.initYearsSlider();
                    this.initPauseButton();
	                this.initCheckboxes();
                    this.initRulesSlider();

                    this.element.on("t5:zone:did-update", function(e){
	                    _this.initTimer();
	                    _this.initImageToggle();
                        _this.initYearsSlider();
                        _this.initPauseButton();
	                    _this.initCheckboxes();
                        _this.initRulesSlider();

	                    _this.element.removeClass("in-update");
                    });

	                this.element.on("t5:zone:refresh", function(e){
		                _this.element.addClass("in-update");

                        _this.element.find(".scale .countdown").countdown("pause");
	                });
                },

                "initTimer" : function(){
                    var _this = this,
	                    timer = $("#hiddenTimer", _this.element);

	                if(timer.length > 0){
		                var time = parseInt(timer.val()),
			                date = new Date(new Date().getTime() + (time * 1000)),
			                progress = _this.element.find(".scale .progress"),
			                countdown = _this.element.find(".scale .countdown");

		                countdown.countdown("destroy").countdown({
			                "until" : date,
			                "format" : "S",
			                "compact" : true,
			                "onTick" : function () {
                                console.log(123);
				                var width = (100 /15 ) * (15 - (countdown.countdown('getTimes')[6]));
								progress.css({"width" : width + "%"});
			                },
			                "onExpiry" : function(){
				                $("#timeIsOverLink").click();
			                }
		                });
	                }

                    return _this;
                },

                "initImageToggle" : function(){
                    var _this = this,
                        handlerElement = _this.element.find(".toggle-image-handler");

	                if(handlerElement.length > 0){
		                handlerElement.click(function(){
			                _this.element.find("img").toggleClass("hidden");
		                });
	                }

	                return _this;
                },

                "initYearsSlider" : function(){
                    var _this = this,
	                    sliderElement = $(".years-slider");

	                if(sliderElement.length > 0){
		                var minInput = $(".minYear-input"),
			                maxInput = $(".maxYear-input"),
			                fromInput = $(".fromYear-input"),
			                toInput = $(".toYear-input");

		                sliderElement.ionRangeSlider({
			                "type" : "double",
			                "min" : minInput.val(),
			                "max" : maxInput.val(),
			                "from" : fromInput.val(),
			                "to" : toInput.val(),
			                "prettify_enabled" : false,
			                "onChange" : function (data) {
				                fromInput.attr({"value" : data.from});
				                toInput.attr({"value" : data.to});
			                }
		                });
	                }

	                return _this;
                },

                "initPauseButton" : function(){
                    var _this = this;

                    _this.element.find(".game-control.pause").click(function(e){

                        console.log("click");

	                    var block = $(this),
		                    setPauseValue = function (value) {
		                    _this.element.find(".variant").each(function(index, element){
			                    var link = $(element),
				                    href = link.attr("href"),
				                    newHref = href.substr(0, href.length - 1) + value;

			                    link.attr({"href" : newHref});
		                    });
	                    };

	                    setPauseValue(block.hasClass("active") ? "0" : "1");
	                    block.toggleClass("active");

                        e.preventDefault();
                    });

	                return _this;
                },

	            "initCheckboxes" : function () {
		            var _this = this;

		            _this.element.find(".checkbox").each(function(index, element){
			            var el = $(element),
				            checkbox = el.find("input[type=checkbox]");

			            if(checkbox.prop("checked")){
				            el.addClass("checked");
			            }
		            });

		            return _this;
	            },

                "initRulesSlider" : function() {
                    var elem = $(".game-start-block");

                    if(elem.length > 0){
                        var swiper = new Swiper('.system-rules .swiper-container', {
                            initialSlide : Math.ceil($(".swiper-slide").length / 2) - 1,
                            effect: 'coverflow',
                            grabCursor: true,
                            nextButton: ".system-rules .nav.next",
                            prevButton: ".system-rules .nav.prev",
                            centeredSlides: true,
                            slidesPerView: 'auto',
                            keyboardControl: true,
                            coverflow: {
                                rotate: 0,
                                stretch: 40,
                                depth: 250,
                                modifier: 2,
                                slideShadows : false
                            },
                            onTransitionEnd : function(swiper){
                                var activeIndex = swiper.activeIndex,
                                    activeElement = elem.find(".swiper-slide").eq(activeIndex),
                                    systemRulesId = activeElement.data("rules-id"),
                                    rulesList = $(".rules-list"),
                                    rulesTypeInput = $("input[name=rulesType]"),
                                    systemRulesIdInput = $("input[name=systemRulesId]");

                                if(activeElement.hasClass("custom-rules-slide")){
                                    rulesList.stop().slideDown(200, "swing");
                                    rulesTypeInput.val("custom");
                                    systemRulesIdInput.val(0);
                                } else {
                                    rulesList.stop().slideUp(200, "swing");
                                    rulesTypeInput.val("system");
                                    systemRulesIdInput.val(systemRulesId);
                                }
                            },

                            onInit : function(swiper){
                                elem.addClass("slider-init");
                            }
                        });
                    }
                }
            };

            return Game;
        }
    )
})(jQuery);