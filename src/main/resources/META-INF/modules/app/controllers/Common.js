(function($){
	define(
		"app/controllers/Common",
		[
		    "app/plugins/Game",
            "app/plugins/jquery/ion.rangeSlider"
		],
		function(Game){

            var gameWindow = $("#gameZone");
            if(gameWindow.length > 0){
                App["game"] = new Game();
            }

            if($(".page-admin.page-rules").length > 0){

                $('body').on('click', ".t-checklist label", function (event) {
                    if( $(event.target).is("label") ) {
                        var el = $(event.target);
                        el.closest(".checkbox").toggleClass("checked");
                    }
                });

                function initYearsSlider() {
                    var sliderElement = $(".years-slider"),
                        minInput = $(".minYear-input"),
                        maxInput = $(".maxYear-input"),
                        fromInput = $(".fromYear-input"),
                        toInput = $(".toYear-input");

                    sliderElement.ionRangeSlider({
                        "type" : "double",
                        "min" : minInput.val(),
                        "max" : maxInput.val(),
                        "from" : fromInput.val(),
                        "to" : toInput.val(),
                        "prettify_enabled" : false,
                        "onChange" : function (data) {
                            fromInput.attr({"value" : data.from});
                            toInput.attr({"value" : data.to});
                        }
                    });
                }

                initYearsSlider();

                $("#rulesFormZone").on("t5:zone:did-update", function () {
                    initYearsSlider();

                    $(".t-checklist input[type='checkbox']").each(function (index, element) {
                        var el = $(element);

                        if(el.prop("checked")){
                            el.closest(".checkbox").addClass("checked");
                        }
                    });
                });
            }
		}
	)
})(jQuery);