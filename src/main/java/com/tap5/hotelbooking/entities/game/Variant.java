package com.tap5.hotelbooking.entities.game;

import javax.persistence.*;

@Entity(name = "variants")
public class Variant {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private int variantNumber;

	private long filmId;

	private String filmName;

	private String alternativeFilmName;

	private int filmReleaseDate;

	@ManyToOne(fetch = FetchType.EAGER)
	private Question question;




	public Variant() {}

	public Variant(int variantNumber, String filmName, String alternativeFilmName, Question question) {
		this.variantNumber = variantNumber;
		this.filmName = filmName;
		this.alternativeFilmName = alternativeFilmName;
		this.question = question;
	}

	public Variant(int variantNumber, String filmName, Question question) {
		this.variantNumber = variantNumber;
		this.filmName = filmName;
		this.question = question;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getVariantNumber() {
		return variantNumber;
	}

	public void setVariantNumber(int variantNumber) {
		this.variantNumber = variantNumber;
	}

	public long getFilmId() {
		return filmId;
	}

	public void setFilmId(long filmId) {
		this.filmId = filmId;
	}

	public String getFilmName() {
		return filmName;
	}

	public void setFilmName(String filmName) {
		this.filmName = filmName;
	}

	public String getAlternativeFilmName() {
		return alternativeFilmName;
	}

	public void setAlternativeFilmName(String alternativeFilmName) {
		this.alternativeFilmName = alternativeFilmName;
	}

	public int getFilmReleaseDate() {
		return filmReleaseDate;
	}

	public void setFilmReleaseDate(int filmReleaseDate) {
		this.filmReleaseDate = filmReleaseDate;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}





	/*
	* Метод проверяет является ли данный вариант горантированно неправильным,
	* при использовании подсказки 50 на 50
	*
	* @return true, если вариант точно неверный, false, если это один из двух осташихся варинтов
	* */

	public boolean isRejected(){
		return variantNumber != getQuestion().getGame().getSecondVariantForFiftyFifty()
				&& (variantNumber != getQuestion().getAnswerNumber());
	}
}
