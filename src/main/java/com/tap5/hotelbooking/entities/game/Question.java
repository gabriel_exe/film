package com.tap5.hotelbooking.entities.game;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;
import java.util.List;


@Entity(name = "questions")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

	private long issuingTime;

    @OneToOne(fetch = FetchType.EAGER)
    private Game game;

    private String imageUrl;

    private String additionalImageUrl;

    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Variant> variants;

    private int answerNumber;


    public Question() {}

    public Question(String imageUrl, String additionalImageUrl, List<Variant> variants, int answerNumber) {
        this.imageUrl = imageUrl;
        this.additionalImageUrl = additionalImageUrl;
        this.variants = variants;
        this.answerNumber = answerNumber;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

	public long getIssuingTime() {
		return issuingTime;
	}

	public void setIssuingTime(long issuingTime) {
		this.issuingTime = issuingTime;
	}

	public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAdditionalImageUrl() {
        return additionalImageUrl;
    }

    public void setAdditionalImageUrl(String additionalImageUrl) {
        this.additionalImageUrl = additionalImageUrl;
    }

    public List<Variant> getVariants() {
        return variants;
    }

    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    public int getAnswerNumber() {
        return answerNumber;
    }

    public void setAnswerNumber(int answerNumber) {
        this.answerNumber = answerNumber;
    }




    public String getFullImgUrl(int image){
        return ((image == 1) ? imageUrl : additionalImageUrl);
    }

    public long getTimeLeft(){
        return 15 - (Instant.now().getEpochSecond() - issuingTime);
    }


}
