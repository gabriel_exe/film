package com.tap5.hotelbooking.entities.game;


import com.tap5.hotelbooking.db.DAO;
import com.tap5.hotelbooking.entities.data.Country;
import com.tap5.hotelbooking.entities.data.Genre;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "rules")
public class Rules {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String type;

	private String name;

	private String description;

	private String background;

	private int minYear;

	private int maxYear;

	private boolean allowedFilms;

	private boolean allowedSerials;

	private boolean allowedMiniSerials;




	@ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
	private List<Genre> genres;

	@ManyToMany(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	private List<Country> countries;


	public Rules() {}

	public Rules(String type, int minYear, int maxYear, List<Genre> genres, List<Country> countries) {
		this.type = type;
        this.minYear = minYear;
        this.maxYear = maxYear;
        this.genres = genres;
		this.countries = countries;

		this.name = "custom";
		this.description = "custom";
    }




    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public int getMinYear() {
		return minYear;
	}

	public void setMinYear(int minYear) {
		this.minYear = minYear;
	}

	public int getMaxYear() {
		return maxYear;
	}

	public void setMaxYear(int maxYear) {
		this.maxYear = maxYear;
	}

	public boolean isAllowedFilms() {
		return allowedFilms;
	}

	public void setAllowedFilms(boolean allowedFilms) {
		this.allowedFilms = allowedFilms;
	}

	public boolean isAllowedSerials() {
		return allowedSerials;
	}

	public void setAllowedSerials(boolean allowedSerials) {
		this.allowedSerials = allowedSerials;
	}

	public boolean isAllowedMiniSerials() {
		return allowedMiniSerials;
	}

	public void setAllowedMiniSerials(boolean allowedMiniSerials) {
		this.allowedMiniSerials = allowedMiniSerials;
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	public List<Country> getCountries() {
		return countries;
	}

	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	public boolean checkFilmsCountOnThisRules(){
		return DAO.getRandomFilmForQuestions(this, true).size() >= 10;
	}

	public String getFullBackgroundPath(){
		return "http://static.v-apelsinah.ru/film/rules_bg/" + background;
	}

	public List<String> getGenresNames(){
		List<String> names = new ArrayList<>();

		for (Genre genre : this.genres){
			names.add(genre.getName());
		}

		return names;
	}

	public List<String> getCountriesNames(){
		List<String> names = new ArrayList<>();

		for (Country country : this.countries){
			names.add(country.getName());
		}

		return names;
	}

	public List<String> getTypes(){
		List<String> types = new ArrayList<>();

		if(isAllowedFilms()){types.add("film");}
		if(isAllowedSerials()){types.add("serial");}
		if(isAllowedMiniSerials()){types.add("mini-serial");}

		return types;
	}
}
