package com.tap5.hotelbooking.entities.game;


import com.tap5.hotelbooking.entities.User;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Entity(name = "games")
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String playerType;

	private String sessionCookie;

	@OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
	private User user;

	private String status;

	private long startTime;

	private long endTime;

	private String reasonCompleted;

	private int answersCount;

	@OneToOne(targetEntity = Question.class, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private Question currentQuestion;

	@OneToOne(targetEntity = Rules.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "rules_id")
	private Rules rules;

	private String tipAdditionalImage;

	private String tipFiftyFifty;

	private int secondVariantForFiftyFifty;

	private boolean tipSkip;



	public Game() {}

	public Game(String playerType, String sessionCookie, User user) {
		this.playerType = playerType;
		this.sessionCookie = sessionCookie;
		this.user = user;

		this.status = "active";
		this.startTime = Instant.ofEpochMilli(new Date().getTime()).getEpochSecond();
		this.endTime = 0;
		this.answersCount = 0;
		this.currentQuestion = null;
		this.rules = null;
		this.tipAdditionalImage = "available";
		this.tipFiftyFifty = "available";
		this.secondVariantForFiftyFifty = 0;
		this.tipSkip = true;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPlayerType() {
		return playerType;
	}

	public void setPlayerType(String playerType) {
		this.playerType = playerType;
	}

	public String getSessionCookie() {
		return sessionCookie;
	}

	public void setSessionCookie(String sessionCookie) {
		this.sessionCookie = sessionCookie;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public String getReasonCompleted() {
		return reasonCompleted;
	}

	public void setReasonCompleted(String reasonCompleted) {
		this.reasonCompleted = reasonCompleted;
	}

	public int getAnswersCount() {
		return answersCount;
	}

	public void setAnswersCount(int answersCount) {
		this.answersCount = answersCount;
	}

	public Question getCurrentQuestion() {
		return currentQuestion;
	}

	public void setCurrentQuestion(Question currentQuestion) {
		this.currentQuestion = currentQuestion;
	}

	public Rules getRules() {
		return rules;
	}

	public void setRules(Rules rules) {
		this.rules = rules;
	}

	public String getTipAdditionalImage() {
		return tipAdditionalImage;
	}

	public void setTipAdditionalImage(String tipAdditionalImage) {
		this.tipAdditionalImage = tipAdditionalImage;
	}

	public String getTipFiftyFifty() {
		return tipFiftyFifty;
	}

	public void setTipFiftyFifty(String tipFiftyFifty) {
		this.tipFiftyFifty = tipFiftyFifty;
	}

	public int getSecondVariantForFiftyFifty() {
		return secondVariantForFiftyFifty;
	}

	public void setSecondVariantForFiftyFifty(int secondVariantForFiftyFifty) {
		this.secondVariantForFiftyFifty = secondVariantForFiftyFifty;
	}

	public boolean isTipSkip() {
		return tipSkip;
	}

	public void setTipSkip(boolean tipSkip) {
		this.tipSkip = tipSkip;
	}




    public void addRules(Rules rules){
        this.setRules(rules);
    }

    public void addCurrentQuestion(Question question){
        question.setGame(this);
        this.setCurrentQuestion(question);
    }



	/*
	* Метод определяет прошло ли отведенное на ответ время.
	*
	* @return true, если время прошло, и false, если время еще есть
	* */
	public boolean isAnswerTimeHasPassed(){
		return (Instant.ofEpochMilli(new Date().getTime()).getEpochSecond() - getCurrentQuestion().getIssuingTime()) >= 15;
	}

	/*
	* Метод переводит подсказки из активного состояния, в недостпуное для использования.
	* */
	public void actualizeTip(){
		if(tipAdditionalImage.equals("active")){
			this.setTipAdditionalImage("notAvailable");
		}

		if(tipFiftyFifty.startsWith("active")){
			this.setTipFiftyFifty("notAvailable");
		}
	}

	/*
	* Метод проверяет а
	* */
    public void complete(String reasonCompleted){
		this.status = "completed";
		this.reasonCompleted = reasonCompleted;
		this.endTime = Instant.ofEpochMilli(new Date().getTime()).getEpochSecond();
	}

	/*
	* Метод возвращает человекочитаемую строку со временем(+ дата) завешения игры
	* */
	public String getPrintedEndTime(String period){

		ZonedDateTime endTime = ZonedDateTime.ofInstant(Instant.ofEpochSecond(getEndTime()), ZoneId.systemDefault());

		DateTimeFormatter dateTimeFormatterDate = DateTimeFormatter.ofPattern("dd.MM.yy");
		DateTimeFormatter dateTimeFormatterTime = DateTimeFormatter.ofPattern("H:mm");

		return dateTimeFormatterDate.format(endTime) + "<br/>" + dateTimeFormatterTime.format(endTime);
	}

	public String getAnswerString(){
		Question question = getCurrentQuestion();
		return question.getVariants().get(question.getAnswerNumber() - 1).getFilmName();
	}
}
