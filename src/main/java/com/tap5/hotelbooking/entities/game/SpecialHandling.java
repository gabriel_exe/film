package com.tap5.hotelbooking.entities.game;


public enum SpecialHandling {
	EXPRESS_SERVICE, GIFT_WRAP, GIFT_BASKET, CUSTOM_ENGRAVING, SHIPPING_INSURANCE, EXTENDED_WARRANTY
}
