package com.tap5.hotelbooking.entities.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity(name = "persons")
public class Person {

	@Id
	@Column(name = "person_id")
	private long id;

	private String name;

	@ManyToMany(mappedBy = "directors")
	private List<Film> madeFilms;

	@ManyToMany(mappedBy = "actors")
	private List<Film> starredInFilms;


	public Person() {}

	public Person(long id, String name) {
		this.id = id;
		this.name = name;
	}




	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Film> getMadeFilms() {
		return madeFilms;
	}

	public void setMadeFilms(List<Film> madeFilms) {
		this.madeFilms = madeFilms;
	}

	public List<Film> getStarredInFilms() {
		return starredInFilms;
	}

	public void setStarredInFilms(List<Film> starredInFilms) {
		this.starredInFilms = starredInFilms;
	}
}
