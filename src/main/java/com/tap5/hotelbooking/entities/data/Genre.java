package com.tap5.hotelbooking.entities.data;

import javax.persistence.*;
import java.util.Set;


@Entity(name = "genres")
public class Genre {

	@Id
    @Column(name = "genre_name")
	private String name;

	@ManyToMany(mappedBy = "genres")
	private Set<Film> films;




	public Genre() {}

	public Genre(String name) {
		this.name = name;
	}

	public Genre(String name, Set<Film> films) {
		this.name = name;
		this.films = films;
	}





	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Film> getFilms() {
		return films;
	}

	public void setFilms(Set<Film> films) {
		this.films = films;
	}

	@Override
	public boolean equals(Object obj) {
		Genre g = (Genre) obj;
		return g.getName().equals(name);
	}
}
