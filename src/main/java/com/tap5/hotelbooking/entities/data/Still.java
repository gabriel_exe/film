package com.tap5.hotelbooking.entities.data;

import com.tap5.hotelbooking.db.DAO;

import javax.imageio.ImageIO;
import javax.persistence.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


@Entity(name = "stills")
public class Still {

	@Id
    @Column(name = "still_id")
	private long id;

	private String pageUrl;

	private String imageUrl;

	private int width;

	private int height;

	private double ratio;

	private long byteSize;

	private boolean availableForGame;

    private String kpType;

	@ManyToOne(cascade = CascadeType.ALL)
	private Film film;




	public Still() {}

    public Still(long id, String pageUrl, String imageUrl, String kpType) {
        this.id = id;
		this.pageUrl = pageUrl;
        this.imageUrl = imageUrl;
		this.kpType = kpType;
    }

    public Still(long id, String imageUrl, String kpType, Film film) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.kpType = kpType;
        this.film = film;
    }






    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public double getRatio() {
		return ratio;
	}

	public void setRatio(double ratio) {
		this.ratio = ratio;
	}

	public long getByteSize() {
		return byteSize;
	}

	public void setByteSize(long byteSize) {
		this.byteSize = byteSize;
	}

	public boolean isAvailableForGame() {
		return availableForGame;
	}

	public void setAvailableForGame(boolean availableForGame) {
		this.availableForGame = availableForGame;
	}

	public String getKpType() {
        return kpType;
    }

    public void setKpType(String kpType) {
        this.kpType = kpType;
    }

    public Film getFilm() {
		return film;
	}

	public void setFilm(Film film) {
		this.film = film;
	}

	public void updateStillParameters(boolean save) throws MalformedURLException, IOException{
		URL url = new URL(imageUrl.replace("//", "http://"));
		final BufferedImage bi = ImageIO.read(url);


		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setRequestMethod("HEAD");

		setHeight(bi.getHeight());
		setWidth(bi.getWidth());
		setRatio((double) width/height);
		setByteSize(urlConnection.getContentLength());
		setAvailableForGame(width > 700 && ratio > 1.5 && ratio < 2);

		if(save){
			DAO.update(this);
		}
	}
}
