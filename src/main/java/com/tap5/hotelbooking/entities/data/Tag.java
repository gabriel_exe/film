package com.tap5.hotelbooking.entities.data;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.List;

@Entity(name = "tags")
public class Tag {

	@Id
	@Column(name = "tag_name")
	private String name;

	@ManyToMany(mappedBy = "tags")
	private List<Film> films;

	public Tag() {}





	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Film> getFilms() {
		return films;
	}

	public void setFilms(List<Film> films) {
		this.films = films;
	}


}
