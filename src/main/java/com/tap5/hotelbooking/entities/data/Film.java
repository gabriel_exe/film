package com.tap5.hotelbooking.entities.data;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;


@Entity(name = "films")
public class Film {

	@Id
	@Column(name = "film_id")
	private long id;

	private String name;

	private String alternativeName;

    private String type;

	private int releaseDate;

	private String premiereWorld;

	private String poster;

	private String bigPoster;

	private String description;

	private String ratingKP;

	private int ratingKPEvaluationsCount;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
		name = "film_tags",
		joinColumns = {@JoinColumn(name = "film_id")},
		inverseJoinColumns = {@JoinColumn(name = "tag_id")}
	)
	private List<Tag> tags;

    @Column(columnDefinition = "TINYINT")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean availableForGame;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
		name = "film_directors",
		joinColumns = {@JoinColumn(name = "film_id")},
		inverseJoinColumns = {@JoinColumn(name = "person_id")})
	private List<Person> directors;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
		name = "film_actors",
		joinColumns = {@JoinColumn(name = "film_id")},
		inverseJoinColumns = {@JoinColumn(name = "person_id")})
	private List<Person> actors;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
		name = "film_countries",
		joinColumns = {@JoinColumn(name = "film_id")},
		inverseJoinColumns = {@JoinColumn(name = "country_id")})
	private List<Country> countries;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
		name = "film_genres",
		joinColumns = {@JoinColumn(name = "film_id")},
		inverseJoinColumns = {@JoinColumn(name = "genre_id")})
	private List<Genre> genres;

	@OneToMany(targetEntity = Still.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "film")
	private List<Still> stills;





	public Film() {}

	public Film(String name, String alternativeName, int releaseDate, List<Country> countries) {
		this.name = name;
		this.alternativeName = alternativeName;
		this.releaseDate = releaseDate;
		this.countries = countries;
	}

	public Film(String name, String alternativeName, int releaseDate, List<Country> countries, List<Genre> filmGenres) {
		this.name = name;
		this.alternativeName = alternativeName;
		this.releaseDate = releaseDate;
		this.countries = countries;
		this.genres = filmGenres;
	}

	public Film(String name, String alternativeName, int releaseDate, List<Country> countries, List<Genre> genres, List<Still> stills) {
		this.name = name;
		this.alternativeName = alternativeName;
		this.releaseDate = releaseDate;
		this.countries = countries;
		this.genres = genres;
		this.stills = stills;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlternativeName() {
		return alternativeName;
	}

	public void setAlternativeName(String alternativeName) {
		this.alternativeName = alternativeName;
	}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(int releaseDate) {
		this.releaseDate = releaseDate;
	}

	public List<Person> getActors() {
		return actors;
	}

	public void setActors(List<Person> actors) {
		this.actors = actors;
	}

	public List<Country> getCountries() {
		return countries;
	}

	public String getPremiereWorld() {
		return premiereWorld;
	}

	public void setPremiereWorld(String premiereWorld) {
		this.premiereWorld = premiereWorld;
	}

	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public String getBigPoster() {
		return bigPoster;
	}

	public void setBigPoster(String bigPoster) {
		this.bigPoster = bigPoster;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRatingKP() {
		return ratingKP;
	}

	public void setRatingKP(String ratingKP) {
		this.ratingKP = ratingKP;
	}

	public int getRatingKPEvaluationsCount() {
		return ratingKPEvaluationsCount;
	}

	public void setRatingKPEvaluationsCount(int ratingKPEvaluationsCount) {
		this.ratingKPEvaluationsCount = ratingKPEvaluationsCount;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public boolean isAvailableForGame() {
        return availableForGame;
    }

    public void setAvailableForGame(boolean availableForGame) {
        this.availableForGame = availableForGame;
    }

    public List<Person> getDirectors() {
		return directors;
	}

	public void setDirectors(List<Person> directors) {
		this.directors = directors;
	}

	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	public List<Still> getStills() {
		return stills;
	}

	public void setStills(List<Still> stills) {
		this.stills = stills;
	}


	public void updateAvailableForGame(){

		if (stills != null && stills.size() >= 2){
			int availableForGameStills = 0;

			for (int i = 0; i < stills.size(); i++) {
				if(stills.get(i).isAvailableForGame()){
					availableForGameStills++;
				}
			}

			availableForGame = availableForGameStills >= 2;

		} else {
			availableForGame = false;
		}
	}
}
