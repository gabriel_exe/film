package com.tap5.hotelbooking.entities.data;

import javax.persistence.*;
import java.util.Set;


@Entity(name = "countries")
public class Country {

	@Id
	@Column(name = "country_name")
	private String name;

	@ManyToMany(mappedBy = "countries")
	private Set<Film> films;


	public Country() {}

	public Country(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Override
	public boolean equals(Object obj) {
		Country c = (Country) obj;
		return c.getName().equals(name);
	}
}
