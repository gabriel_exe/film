package com.tap5.hotelbooking.services.parser.checker;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


public class PageChecker {

	public static boolean check(Document document, PageTypes type){
		switch (type){
			case FILM_MAIN: return isFilmMain(document);
			case FILM_MAIN_OR_404: return isFilmMainOr404(document);
			case PAGE_404: return isPage404(document);
			case STILLS_PREVIEW: return isStillsPreview(document);
			case SCREENSHOTS_PREVIEW: return isScreenshotsPreview(document);
			case PICTURE: return isPicture(document);
			default: return false;
		}
	}

	private static boolean isFilmMain(Document document){
		Elements photoInfoTableElements = document.select("#photoInfoTable");
		if(photoInfoTableElements.size() != 0){
			Elements headerFilmElements = photoInfoTableElements.select("#headerFilm");
			if(headerFilmElements.size() != 0){
				return true;
			}
		}
		return false;
	}

	private static boolean isFilmMainOr404(Document document){

		Elements headerFilmElements = document.select("#photoInfoTable #headerFilm");
		if(headerFilmElements.size() != 0){
			return true;
		} else if (document.select("html#page404_hash132119891").size() != 0){
			return true;
		}

		return false;
	}

	private static boolean isPage404(Document document){
		return document.select("html#page404_hash132119891").size() != 0;
	}

	private static boolean isStillsPreview(Document document){
		Elements elements = document.select("ul.breadcrumbs .breadcrumbs__link:contains(Кадры)");
		return (elements.size() != 0);
	}

	private static boolean isScreenshotsPreview(Document document){
		Elements elements = document.select("ul.breadcrumbs .breadcrumbs__link:contains(Скриншоты)");
		return (elements.size() != 0);
	}

	private static boolean isPicture(Document document){
		Elements elements = document.select("table.thermo td:contains(Просмотр фото)");
		return (elements.size() != 0);
	}
}
