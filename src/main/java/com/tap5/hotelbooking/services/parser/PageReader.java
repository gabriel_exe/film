package com.tap5.hotelbooking.services.parser;

import com.tap5.hotelbooking.services.parser.checker.PageChecker;
import com.tap5.hotelbooking.services.parser.checker.PageTypes;
import com.tap5.hotelbooking.services.proxy.ProxyHelper;
import com.tap5.hotelbooking.services.proxy.ProxyStorage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.concurrent.Callable;

public class PageReader implements Callable<Document> {

	final static Logger logger = LoggerFactory.getLogger(PageReader.class);

	private String urlString;

	private PageTypes type;


	public PageReader(String url, PageTypes type) {
		this.urlString = url;
		this.type = type;
	}


	public String getUrl() {
		return urlString;
	}

	public void setUrl(String urlString) {
		this.urlString = urlString;
	}


	@Override
	public Document call() throws ProxyHelper.ProxyUsingException {

		String proxyString = ProxyStorage.getRandomProxy();
		logger.debug("выбрана прокси:" + proxyString);
		Document document;

		if(proxyString != null){
			String hostName = proxyString.split(":")[0];
			int port = (Integer.valueOf(proxyString.split(":")[1]));

			try {
				URL url = new URL(urlString);

				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(hostName, port));
				HttpURLConnection connection = (HttpURLConnection) url.openConnection(proxy);

				connection.setRequestMethod("GET");
				connection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
				connection.setRequestProperty("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
				connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36");

				connection.setConnectTimeout(3000);
				connection.setReadTimeout(3000);


				if(connection.getResponseCode() != 404){
					String line;
					StringBuffer pageContent = new StringBuffer();
					BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("windows-1251")));
					while ((line = in.readLine()) != null) {
						pageContent.append(line);
					}

					document = Jsoup.parse(String.valueOf(pageContent));
				} else {
					document = Jsoup.parse(String.valueOf("<html id=\"page404_hash132119891\"></html>"));
				}

				String blockMark = "Если вы видите эту страницу, значит с вашего IP-адреса поступило необычно много запросов.";

				if(document.select("td:contains(" + blockMark + ")").size() > 0){
					ProxyStorage.rejectProxy(proxyString);
					return null;
				} else {
					logger.debug("Рабочая прокси:" + proxyString);

					if(PageChecker.check(document, type)){
						logger.debug("Документ получен успешно.");
						return document;
					} else {
						logger.debug("Документ получен, но это не то документ что мы ищем.");
						return null;
					}
				}
			} catch (IOException e) {
				logger.debug("Ошибка чтения документа с помощью прокси: " + proxyString, e);
				return  null;
			}
		} else {
			return null;
		}
	}
}
