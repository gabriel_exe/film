package com.tap5.hotelbooking.services.parser.checker;


public enum PageTypes {

	FILM_MAIN,
	FILM_MAIN_OR_404,
	PAGE_404,
	STILLS_PREVIEW,
	SCREENSHOTS_PREVIEW,
	PICTURE
}
