package com.tap5.hotelbooking.services.parser;

import com.tap5.hotelbooking.services.proxy.ProxyHelper;
import com.tap5.hotelbooking.entities.data.Still;
import com.tap5.hotelbooking.services.parser.checker.PageTypes;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;

public class StillSaver implements Callable<Still> {

    final static Logger logger = LoggerFactory.getLogger(StillSaver.class);

    String BASE_BATH = "C:\\static\\film\\stills";

    private String stillUrl;
	private String type;

    public StillSaver(String stillUrl, String type) {
        this.stillUrl = stillUrl;
		this.type = type;
    }

    public Still call() throws ProxyHelper.ProxyUsingException{

        try{
            Document document = Parser.getPageDocument("https://www.kinopoisk.ru" + stillUrl, PageTypes.PICTURE);
            String imageSrc = document.select("#image").attr("src");
			Still still = new Still(Long.valueOf(stillUrl.split("/")[2]), "https://www.kinopoisk.ru" + stillUrl, imageSrc, type);
            still.updateStillParameters(false);
			return still;
        } catch (ProxyHelper.ProxyUsingException e){
			throw e;
		} catch (Exception e){
            logger.error("Ошибка сохранения кадра " + stillUrl, e );
        }

        return null;
    }
}
