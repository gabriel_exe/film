package com.tap5.hotelbooking.services.parser;

import com.tap5.hotelbooking.entities.data.*;
import com.tap5.hotelbooking.services.BaseExecutorService;
import com.tap5.hotelbooking.services.parser.checker.PageChecker;
import com.tap5.hotelbooking.services.parser.checker.PageTypes;
import com.tap5.hotelbooking.services.proxy.ProxyHelper;
import com.tap5.hotelbooking.services.proxy.ProxyStorage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Parser implements Callable<Film>{

    final static Logger logger = LoggerFactory.getLogger(Parser.class);

    private long filmNumber;

    private Document indexPage;

	@Override
	public Film call() throws Exception {
		return parse();
	}

	public Parser(long filmNumber) {
        this.filmNumber = filmNumber;
    }







    public long getFilmNumber() {
        return filmNumber;
    }

    public void setFilmNumber(long filmNumber) {
        this.filmNumber = filmNumber;
    }

    public static Logger getLogger() {
        return logger;
    }


	public static Document getPageDocumentWithProxy(String urlString, String hostName, int port) throws IOException, ProxyHelper.ProxyUsingException{

        String proxyString = hostName + ":" + port;

        try {
            URL url = new URL(urlString);

            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(hostName, port));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection(proxy);
            connection.setConnectTimeout(3000);
            connection.setReadTimeout(3000);

            String line;
            StringBuffer pageContent = new StringBuffer();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((line = in.readLine()) != null) {
                pageContent.append(line);
            }

            Document document = Jsoup.parse(String.valueOf(pageContent));

            String blockMark = "Если вы видите эту страницу, значит с вашего IP-адреса поступило необычно много запросов.";

            if(document.select("td:contains(" + blockMark + ")").size() > 0){
                ProxyStorage.rejectProxy(proxyString);
                return  getPageDocumentWithProxy(urlString, hostName, port);
            } else {
                return document;
            }
        } catch (IOException e){
            return  getPageDocumentWithProxy(urlString, hostName, port);
        }

	}

    public static Document getPageDocument(String urlString, PageTypes type) throws ProxyHelper.ProxyUsingException{

		Document document = null;

		Set<Future<Document>> futures = new HashSet<>();

		for (int i = 0; i < 10; i++) {
			futures.add(BaseExecutorService.getInstance().submit(new PageReader(urlString, type)));
		}

		for(Future<Document> future : futures){
			try {

				document = future.get();

				if(document != null){
					break;
				}

			} catch (InterruptedException | ExecutionException e){
				logger.error("Ошибка:", e);
			}
		}

		if(document == null){
			return getPageDocument(urlString, type);
		} else{
			return document;
		}
    }

    public boolean isFilmPage() throws IOException, ProxyHelper.ProxyUsingException{
		Document document = getPageDocument("https://www.kinopoisk.ru/film/" + this.filmNumber +"/", PageTypes.FILM_MAIN_OR_404);
		return PageChecker.check(document, PageTypes.PAGE_404);
    }

    public List<Country> parseFilmCountries(){

        try{
            Elements elements = indexPage.select("#viewFilmInfoWrapper table.info td:contains(страна) + td div a");
            List<Country> countries = new ArrayList<>();

            for (Element element : elements) {
                countries.add(new Country(element.text()));
            }
            return countries;
        } catch (Exception e){
            logger.error("Ошибка получения списка стран для фильма №" + this.filmNumber, e);
        }

        return null;
    }

    public String parseFilmName(){

        try{
			Elements elements = indexPage.select("#viewFilmInfoWrapper h1.moviename-big");
            if(elements.size() != 0){
                Elements clone = elements.clone();
                clone.get(0).select("span").remove();
                return clone.text();
            }
        } catch (Exception e){
            logger.error("Ошибка получения имени для фильма №" + this.filmNumber, e);
        }

        return null;
    }

    public String parseAlternativeFilmName(){

        try{
            return indexPage.select("#viewFilmInfoWrapper [itemprop=alternativeHeadline]").text();
        } catch (Exception e){
            logger.error("Ошибка получения английского имени для фильма №" + this.filmNumber, e);
        }

        return null;
    }

    public String parseType(){
        try{
            if(indexPage.select("#viewFilmInfoWrapper .moviename-big :contains(мини-сериал)").size() != 0){
                return "mini-serial";
            } else if(indexPage.select("#viewFilmInfoWrapper .moviename-big :contains(сериал)").size() != 0){
                return "serial";
            } else {
                return "film";
            }
        } catch (Exception e){
            logger.error("Ошибка получения типа фильма фильма №" + this.filmNumber, e);
        }

        return null;
    }

    public int parseReleaseDate(){

        try{
			Elements elements = indexPage.select("#viewFilmInfoWrapper table.info td:contains(год) + td div a");
			return elements.size() != 0 ? Integer.valueOf(elements.first().text()) : 0;
        } catch (Exception e){
            logger.error("Ошибка получения даты релиза для фильма №" + this.filmNumber, e);
        }

        return 0;
    }

    public String parsePremierWorld(){
        try{
            Elements elements = indexPage.select("#div_world_prem_td2 a");
            return (elements.size() == 0) ? null : elements.first().text();
        } catch (Exception e){
            logger.error("Ошибка получения даты премьеры в мире для фильма №" + this.filmNumber, e);
        }

        return null;
    }

    public String parsePoster(){

        try{

            Elements images = indexPage.select("#photoBlock .film-img-box img");

            if(images.size() != 0){
                Element img = images.first();
				return img.attr("src");
            }
        } catch (Exception e){
            logger.error("Ошибка сохранения постера для фильма " + this.filmNumber, e);
        }

        return null;
    }

    public String parseBigPoster(){
        try{

            Elements links = indexPage.select("#photoBlock .popupBigImage");
            if(links.size() != 0){

                Element link = links.first();
                String onclick = link.attr("onclick");

                if(!onclick.equals("")){
                    String imgUrl = onclick.substring(onclick.indexOf("'") + 1, onclick.lastIndexOf("'"));
                    return "https://www.kinopoisk.ru" + imgUrl;
                } else {
                    return "notBigPoster";
                }

            } else {
                return "notBigPoster";
            }

        } catch (Exception e){
            logger.error("Ошибка сохранения большого постера для фильма " + this.filmNumber, e);
        }

        return null;
    }

	public String parseDescription(){

		try{
			if(indexPage.select("#block_left_padtop ._reachbanner_ .brand_words").size() != 0){
				return indexPage.select("#block_left_padtop ._reachbanner_ .brand_words").get(0).text();
			}
		} catch (Exception e){
			logger.error("Ошибка получения описания для фильма " + this.filmNumber, e);
		}

		return null;
	}

	public String parseRatingKP(){
		try{
			if(indexPage.select("#block_rating .block_2 .rating_ball").size() != 0){
				return indexPage.select("#block_rating .block_2 .rating_ball").get(0).text();
			}
		} catch (Exception e){
			logger.error("Ошибка получения рейтинга кинопоиска для фильма №" + this.filmNumber, e);
		}

		return null;
	}

	public int parseRatingKPEvaluationsCount(){

		try{
			if(indexPage.select("#block_rating .block_2 [itemprop=ratingCount]").size() != 0){
				String text = indexPage.select("#block_rating .block_2 [itemprop=ratingCount]").get(0).text();
				return (text.equals("\u00a0"))
						? 0 :
						Integer.parseInt(text.replaceAll("\\D", ""));
			}
		} catch (Exception e){
			logger.error("Ошибка получения колличества оценок для рейтинга кинопоиска для фильма №" + this.filmNumber, e);
		}

		return 0;
	}


    public List<Person> parseFilmDirectors(){

        try {
            Elements elements = indexPage.select("#viewFilmInfoWrapper table.info td:contains(режиссер) + td a");
            List<Person> directors = new ArrayList<>();

            for (Element element : elements) {
                String personName = element.text();
                if(!personName.equals("...")){
                    String link = element.attr("href");
                    long peronId = Integer.valueOf(link.split("/")[2]);

                    Person person = new Person(peronId, personName);

                    directors.add(person);
                }
            }

            return directors;
        } catch (Exception e){
            logger.error("Ошибка получения списа режиссёров для фильма №" + this.filmNumber, e);
        }

        return null;
    }

    public List<Person> parseActors(){

        List<Person> actors = new ArrayList<>();
        Elements links = indexPage.select("#actorList h4:contains(В главных ролях:) + ul li a");

        for (Element link : links){
            String name = link.text();
            if(!name.equals("...")){
                long id = Integer.parseInt(link.attr("href").split("/")[2]);
                actors.add(new Person(id, name));
            }
        }

        return actors;

    }

    public List<Genre> parseFilmGenres(){

        try{
            Elements elements = indexPage.select("#viewFilmInfoWrapper table.info td:contains(жанр) + td [itemprop=genre] a");
            List<Genre> genres = new ArrayList<>();

            for (Element element : elements) {
                Genre genre = new Genre(element.text());
                genres.add(genre);
            }

            return genres;

        } catch (Exception e){
            logger.error("Ошибка получения списа жанров для фильма №" + this.filmNumber, e);
        }

        return null;
    }

    public List<Still> saveStillsAndScreenShots(Film film) throws ProxyHelper.ProxyUsingException{

		List<String> stillLinksSet = new ArrayList<>();


        try{
            Element stillElement = indexPage.select("#newMenuSub span:contains(кадры)").get(0);
            Element stillElementParent = stillElement.parent();

            if(stillElementParent.tag().getName().equals("a")){

                String link = "https://www.kinopoisk.ru/film/" + filmNumber + "/stills/";
                Document stillsPreviewPage = Parser.getPageDocument(link, PageTypes.STILLS_PREVIEW);
                Element firstElementLink = stillsPreviewPage.select("table.fotos a").first();
                String firstLink = firstElementLink.attr("href");

                Document stillPageWithList = Parser.getPageDocument("https://www.kinopoisk.ru" + firstLink, PageTypes.PICTURE);
                Elements stillsLinksElements = stillPageWithList.select("#pages > a");


				stillLinksSet.add(firstLink);
                for(Element element : stillsLinksElements){
                    stillLinksSet.add(element.attr("href"));
                }
            }
        } catch (ProxyHelper.ProxyUsingException e){
			throw e;
		} catch (Exception e){
            logger.error("Ошибка поулчения списка кадров для фильма " + this.filmNumber, e);
        }


		List<String> screenshotsLinksSet = new ArrayList<>();

        try {
            Elements screenShotElements = indexPage.select("#newMenuSub a:contains(скриншоты)");
            if(screenShotElements.size() != 0){
                Element screenShotElement = screenShotElements.get(0);

                if(screenShotElement.tag().getName().equals("a")){

                    String link = "https://www.kinopoisk.ru/film/" + this.filmNumber + "/screenshots/";
                    Document screenshotsPreviewPage = Parser.getPageDocument(link, PageTypes.SCREENSHOTS_PREVIEW);
                    Element firstElementLink = screenshotsPreviewPage.select("table.fotos a").first();
                    String firstLink = firstElementLink.attr("href");

                    Document screenshotsPageWithList = Parser.getPageDocument("https://www.kinopoisk.ru" + firstLink, PageTypes.PICTURE);
                    Elements screenshotsLinksElements = screenshotsPageWithList.select("#pages > a");


                    screenshotsLinksSet.add(firstLink);
                    for(Element element : screenshotsLinksElements){
						screenshotsLinksSet.add(element.attr("href"));
                    }
                }
            }
        } catch (ProxyHelper.ProxyUsingException e){
			throw e;
		} catch (Exception e){
            logger.error("Ошибка получения списка скриншотов для фильма" + this.filmNumber, e);
        }

		Set<Future<Still>> futures = new HashSet<>();

		for(String s : stillLinksSet){
			futures.add(BaseExecutorService.getInstance().submit(new StillSaver(s, "still")));
		}

		for(String s : screenshotsLinksSet){
			futures.add(BaseExecutorService.getInstance().submit(new StillSaver(s, "screenshot")));
		}


		List<Still> stills = new ArrayList<>();

		for(Future<Still> future : futures){
			try {
				Still still = future.get();
				if(still != null){
					still.setFilm(film);
					stills.add(still);
				}
			} catch (InterruptedException | ExecutionException e){
				logger.error("Ошибка получения картинки для фильма" + this.filmNumber, e);
			}
		}

	    /*BaseExecutorService.getInstance().shutdown();*/

        return (stills.size() == 0) ? null : stills;
    }




    public Film parse() throws ProxyHelper.ProxyUsingException{

        try {

			logger.info("Ничинаем разбирать фильм №" + filmNumber);

            Film film = new Film();
            film.setId(filmNumber);

            String urlString = "https://www.kinopoisk.ru/film/" + this.filmNumber +"/";
			this.indexPage = getPageDocument(urlString, PageTypes.FILM_MAIN_OR_404);

            if(!PageChecker.check(indexPage, PageTypes.PAGE_404)){
                film.setName(parseFilmName());
                film.setAlternativeName(parseAlternativeFilmName());
                film.setType(parseType());
                film.setDirectors(parseFilmDirectors());
                film.setReleaseDate(parseReleaseDate());
                film.setPoster(parsePoster());
                film.setBigPoster(parseBigPoster());
				film.setDescription(parseDescription());
				film.setRatingKPEvaluationsCount(parseRatingKPEvaluationsCount());
                film.setPremiereWorld(parsePremierWorld());
                film.setCountries(parseFilmCountries());
                film.setGenres(parseFilmGenres());
				film.setRatingKP(parseRatingKP());
                //film.setActors(parseActors());
	            if(film.getRatingKPEvaluationsCount() > 10000){
		            film.setStills(saveStillsAndScreenShots(film));
	            }

				film.updateAvailableForGame();


                logger.info("Фильм №" + filmNumber + " успешно разобран");
            } else {
                logger.warn("Фильм №" + filmNumber + " не найден");
                film.setName("PAGE_FILM_DOES_NOT_EXIST");
            }

            return film;
        } catch (ProxyHelper.ProxyUsingException e){
			throw e;
		} catch (Exception e){
            logger.error("Общая ошибка разбора фильма № " + this.filmNumber, e);
            Film film = new Film();
            film.setId(filmNumber);
            film.setName("GLOBAL_PARSE_ERROR");
            return film;
        }
    }
}
