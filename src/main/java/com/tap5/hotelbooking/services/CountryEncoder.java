package com.tap5.hotelbooking.services;

import com.tap5.hotelbooking.db.DAO;
import com.tap5.hotelbooking.entities.data.Country;
import org.apache.tapestry5.ValueEncoder;
import org.apache.tapestry5.services.ValueEncoderFactory;


public class CountryEncoder implements ValueEncoder<Country>, ValueEncoderFactory {

	public CountryEncoder(){}

	@Override
	public String toClient(Country value) {
		return value.getName();
	}

	@Override
	public Country toValue(String clientValue) {
		return DAO.getByStringId(Country.class, clientValue);
	}

	@Override
	public ValueEncoder create(Class type) {
		return this;
	}
}
