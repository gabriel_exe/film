package com.tap5.hotelbooking.services;

import com.tap5.hotelbooking.db.DAO;
import com.tap5.hotelbooking.entities.data.Film;
import com.tap5.hotelbooking.entities.data.Still;
import com.tap5.hotelbooking.entities.game.Question;
import com.tap5.hotelbooking.entities.game.Rules;
import com.tap5.hotelbooking.entities.game.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class GameService {

    final static Logger logger = LoggerFactory.getLogger(GameService.class);

    public static Question createRandomQuestion(Rules rules){

        Question question = new Question();
		question.setIssuingTime(Instant.ofEpochMilli(new Date().getTime()).getEpochSecond());

        List<Film> films = DAO.getRandomFilmForQuestions(rules, false);


        List<Variant> variants = new ArrayList<>();

        int i = 0;
        for(Film film :films){
            Variant variant = new Variant();
            variant.setVariantNumber(i + 1);
            variant.setFilmName(film.getName());
            variant.setAlternativeFilmName(film.getAlternativeName());
            variant.setFilmId(film.getId());
            variant.setFilmReleaseDate(film.getReleaseDate());
            variant.setQuestion(question);

            variants.add(variant);
            i++;
        }

        question.setVariants(variants);


        int answerNumber = new Random().nextInt(4) + 1;
        Film answerFilm = films.get(answerNumber - 1);
	    List<Still> availableForGameStills = new ArrayList<>();
	    for (int j = 0; j < answerFilm.getStills().size(); j++) {
		    Still st = answerFilm.getStills().get(j);
		    if(st.isAvailableForGame()){
			    availableForGameStills.add(st);
		    }
	    }
        int stillsCount = availableForGameStills.size();
        int[] ints = ThreadLocalRandom.current().ints(0, stillsCount).distinct().limit(2).toArray();
        question.setImageUrl(availableForGameStills.get(ints[0]).getImageUrl());
        question.setAdditionalImageUrl(availableForGameStills.get(ints[1]).getImageUrl());
        question.setAnswerNumber(answerNumber);

        return question;
    }
}
