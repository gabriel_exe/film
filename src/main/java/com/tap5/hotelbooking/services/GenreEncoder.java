package com.tap5.hotelbooking.services;

import com.tap5.hotelbooking.db.DAO;
import com.tap5.hotelbooking.entities.data.Genre;
import org.apache.tapestry5.ValueEncoder;
import org.apache.tapestry5.services.ValueEncoderFactory;


public class GenreEncoder implements ValueEncoder<Genre>, ValueEncoderFactory {

	public GenreEncoder(){}

	@Override
	public String toClient(Genre value) {
		return value.getName();
	}

	@Override
	public Genre toValue(String clientValue) {
		return DAO.getByStringId(Genre.class, clientValue);
	}

	@Override
	public ValueEncoder create(Class type) {
		return this;
	}
}
