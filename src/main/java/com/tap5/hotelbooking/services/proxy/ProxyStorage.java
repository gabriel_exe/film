package com.tap5.hotelbooking.services.proxy;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class ProxyStorage {

	private final static Logger logger = LoggerFactory.getLogger(ProxyStorage.class);

	private final static String DEV_PATH = "C:/proxy/";
	private final static String PRODUCTION_PATH = "/home/gabriel/proxy/";
	private final static String SOURCE_FOR_PARSE = "proxy_for_parse.txt";

	private static ProxyStorage ourInstance;

	private static List<String> availableProxies;

	public static ProxyStorage getInstance() {
		if(ourInstance == null){
			ourInstance = new ProxyStorage();
		}
		return ourInstance;
	}

	private ProxyStorage() {
		try {
			availableProxies =  Files.readAllLines(Paths.get(getPath() + SOURCE_FOR_PARSE));
		} catch (IOException e){
			availableProxies = null;
		}
	}

	public static String getPath(){
		return (System.getProperty("environment.type").equals("dev")) ? DEV_PATH : PRODUCTION_PATH;
	}

	public static String getRandomProxy() throws ProxyHelper.ProxyUsingException {
		if(availableProxies.size() != 0){
			return availableProxies.get(new Random().nextInt(availableProxies.size()));
		} else {
			throw new ProxyHelper.ProxyUsingException();
		}
	}

	public static void rejectProxy(String proxy){
		logger.error("Забаненная прокси: " + proxy);
		availableProxies.remove(proxy);
		saveAvailableProxies();
	}

	private static void saveAvailableProxies(){

		logger.error("Запущена таска на сохранение");
		try {
			Files.write(
				Paths.get(getPath() + SOURCE_FOR_PARSE),
				String.join("\n", availableProxies).getBytes(),
				StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING
			);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
