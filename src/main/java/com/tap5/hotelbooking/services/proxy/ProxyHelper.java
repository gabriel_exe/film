package com.tap5.hotelbooking.services.proxy;

import org.apache.commons.io.FileUtils;
import org.apache.http.client.utils.URIBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class ProxyHelper {


	private static ProxyHelper proxyHelper;

	private final static Logger logger = LoggerFactory.getLogger(ProxyHelper.class);


	private ProxyHelper(){}


	public static ProxyHelper getInstance(){

		if(proxyHelper == null){
			proxyHelper = new ProxyHelper();
		}

		return proxyHelper;
	}


	public static void validateProxy(String unvalidatedFile, String validatedFile, int poolSize) {

		try {

			List<String> proxiesList = Files.readAllLines(Paths.get(unvalidatedFile));
			Set<String> proxiesSet = new HashSet<>(proxiesList);
			proxiesList.clear();
			proxiesList.addAll(proxiesSet);


			ExecutorService executorService = Executors.newFixedThreadPool(poolSize);
			Set<Future<ProxyChecker.CheckResult>> futuresSet = new HashSet<>();

			for(int i = 0; i < proxiesList.size(); i++){
				String string = proxiesList.get(i);
				String ip = string.trim().split(":")[0];
				int port = Integer.valueOf(string.trim().split(":")[1]);

				futuresSet.add(executorService.submit(new ProxyChecker(i, ip, port)));
			}


			int validated = 0;
            int banned = 0;
			List<String> success = new ArrayList<>();

			for (Future<ProxyChecker.CheckResult> future : futuresSet){
				ProxyChecker.CheckResult result = future.get();
				if(result.isResult()){
					String string = result.getIp() + ":" + result.getPort();
					success.add(string);
					logger.info("Рабочая прокси: " + string);
				}

                if(result.isBanned()){
                    banned++;
                }
				validated++;
			}


			Files.write(
				Paths.get(validatedFile),
				String.join("\n", success).getBytes(),
				StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING
			);

			logger.info("Валидация завершена. Проверено " + validated + ", из них успешно " + success.size() + ". Забанено: " + banned);

		} catch (Exception e){
			logger.error("Неудалось завершить валидацию " + e.toString());
		}
	}





	public static void pullProxyByOrcaHub(){

		try {

			Document document = Jsoup.parse(readDocumentByURL("https://orca.tech/community-proxy-list/"));
			Elements filesLinks = document.select("a");

			Set<String> commonSet = new HashSet<>();

			for (Element link : filesLinks){

				String remoteFile = link.attr("href");
				String localFile = "C:\\proxy\\temp\\" + remoteFile;
				URL url = new URL("https://orca.tech/community-proxy-list/" + remoteFile);
				HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
				httpURLConnection.setRequestProperty("Cookie", "challenge=https://Orca.Tech");

				FileUtils.copyInputStreamToFile(httpURLConnection.getInputStream(), new File(localFile));

				List list = FileUtils.readLines(new File(localFile));
				List freshList = list.subList(1, list.size() - 2);

				commonSet.addAll(freshList);

				FileUtils.deleteQuietly(new File(localFile));
			}

			for(Iterator<String> roleIter = commonSet.iterator(); roleIter.hasNext();){
				String address = roleIter.next();
				if (!address.trim().matches("[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}:[0-9]{1,6}")) {
					roleIter.remove();
				}
			}

			Files.write(
				Paths.get("C:\\proxy\\unvalidated\\orcahub.txt"),
				String.join("\n", commonSet).getBytes(),
				StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING
			);
			logger.info("C Orcahub.net получено " + commonSet.size() + " прокси.");
		} catch (IOException e) {
			logger.error("Ошибка получения списка прокси с Orcahub.net: " + e.toString());
		}
	}

	public static void pullProxyByFoxtools(){
		try {

			JSONObject object = pullPartialProxyByFoxtools(buildFoxtoolsURL(1));
			JSONObject response = (JSONObject) object.get("response");
			JSONArray items =  (JSONArray) response.get("items");

			int pageCount = response.getInt("pageCount");

			if (pageCount > 1){
				for (int pageNumber = 2; pageNumber <= pageCount; pageNumber++){
					JSONObject additionalObject = pullPartialProxyByFoxtools(buildFoxtoolsURL(pageNumber));
					JSONObject additionalResponse = (JSONObject) additionalObject.get("response");
					JSONArray additionalItems =  (JSONArray) additionalResponse.get("items");

					for(int i = 0; i < additionalItems.length(); i++){
						items.put(additionalItems.get(i));
					}
				}
			}

			Set commonSet = new HashSet<>();
			for (int i = 0; i < items.length(); i++){
				JSONObject item = (JSONObject) items.get(i);
				commonSet.add(item.get("ip") + ":" + item.get("port"));
			}

			Files.write(
				Paths.get("C:\\proxy\\unvalidated\\foxtools.txt"),
				String.join("\n", commonSet).getBytes(),
				StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING
			);

			logger.info("C Foxtools получено " + commonSet.size() + " прокси.");
		} catch (Exception e){
			logger.error("Ошибка получения списка прокси с fooxtools.ru: " + e.toString());
		}
	}

	public static JSONObject pullPartialProxyByFoxtools(URL url) throws IOException, JSONException {
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

		urlConnection.connect();

		String line = null;
		StringBuffer tmp = new StringBuffer();
		BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		while ((line = in.readLine()) != null) {
			tmp.append(line);
		}

		return new JSONObject(String.valueOf(tmp));
	}



	public static URL buildFoxtoolsURL(int page) throws URISyntaxException, MalformedURLException{
		URIBuilder b = new URIBuilder("http://api.foxtools.ru/v2/Proxy");
		b.addParameter("cp", "UTF-8");
		b.addParameter("lang", "RU");
		b.addParameter("type", "HTTP");
		b.addParameter("anonymity", "All");
		b.addParameter("available", "Yes");
		b.addParameter("free", "Yes");
		b.addParameter("limit", "100");
		b.addParameter("uptime", "2");
		b.addParameter("formatting", "2");
		b.addParameter("page", String.valueOf(page));

		return b.build().toURL();
	}

	public static String readDocumentByURL(String url) throws IOException{

		String pageString = null;

		try{
			URL commonListsUrl = new URL(url);
			URLConnection connection = (url.startsWith("https://"))
				? (HttpsURLConnection) commonListsUrl.openConnection()
				: (HttpURLConnection) commonListsUrl.openConnection();

			connection.setRequestProperty("Cookie", "challenge=https://Orca.Tech");

			String line;
			StringBuffer pageContent = new StringBuffer();
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while ((line = in.readLine()) != null) {
				pageContent.append(line);
			}

			pageString = String.valueOf(pageContent);

		} catch (IOException e){
			logger.error("Ошибка чтения документа по URL: " + url + ". Причина: " + e.toString());
			throw e;
		}

		return pageString;

	}

	public static class ProxyChecker implements Callable<ProxyChecker.CheckResult> {

		private int number;

		private String ip;

		private int port;



		public ProxyChecker(int number, String ip, int port){
			this.number = number;
			this.ip = ip;
			this.port = port;
		}




		public String getIp() {
			return ip;
		}

		public void setIp(String ip) {
			this.ip = ip;
		}

		public int getPort() {
			return port;
		}

		public void setPort(int port) {
			this.port = port;
		}



		public CheckResult call() {

			CheckResult result = new CheckResult(number, ip, port);

			try{
				URL url = new URL("https://www.kinopoisk.ru/film/298/screenshots/");
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ip, port));
				HttpURLConnection connection = (HttpURLConnection) url.openConnection(proxy);
				System.out.println(proxy);
				connection.setRequestMethod("GET");
				connection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
				connection.setRequestProperty("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
				connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36");
				connection.setConnectTimeout(5000);
				connection.setReadTimeout(5000);
				connection.connect();

				String line;
				StringBuffer tmp = new StringBuffer();
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				while ((line = in.readLine()) != null) {
					tmp.append(line);
				}

				Document document = Jsoup.parse(String.valueOf(tmp));

				Elements elements = document.select("#footer_wrapper #footer");

				if(elements.size() != 0){
					result.setResult(true);
				} else {
					result.setResult(false);
				}

                String blockMark = "Если вы видите эту страницу, значит с вашего IP-адреса поступило необычно много запросов.";
                if(document.select("td:contains(" + blockMark + ")").size() > 0){
                    result.setBanned(true);
                } else {
                    result.setBanned(false);
                }

				return result;

			} catch (Exception e){
				logger.error("Ошибка проверки прокси №" + number + ": " + e.toString());
				result.setResult(false);
				return result;
			}
		}

		public class CheckResult{

			private int number;

			private String ip;

			private int port;

			private boolean result;

            private boolean banned;


			public CheckResult() {}

			public CheckResult(int number, String ip, int port) {
				this.number = number;
				this.ip = ip;
				this.port = port;
			}


			public int getNumber() {
				return number;
			}

			public void setNumber(int number) {
				this.number = number;
			}

			public String getIp() {
				return ip;
			}

			public void setIp(String ip) {
				this.ip = ip;
			}

			public int getPort() {
				return port;
			}

			public void setPort(int port) {
				this.port = port;
			}

			public boolean isResult() {
				return result;
			}

			public void setResult(boolean result) {
				this.result = result;
			}

            public boolean isBanned() {
                return banned;
            }

            public void setBanned(boolean banned) {
                this.banned = banned;
            }
        }
	}

	public static class ProxyUsingException extends Exception{
		public ProxyUsingException() {
		}

		public ProxyUsingException(String message) {
			super(message);
		}

		public ProxyUsingException(String message, Throwable cause) {
			super(message, cause);
		}

		public ProxyUsingException(Throwable cause) {
			super(cause);
		}

		public ProxyUsingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
			super(message, cause, enableSuppression, writableStackTrace);
		}
	}
}
