package com.tap5.hotelbooking.services;

import com.tap5.hotelbooking.entities.User;
import com.tap5.hotelbooking.security.AuthenticationException;


public interface Authenticator
{


    User getLoggedUser();


    boolean isLoggedIn();


    void login(String username, String password) throws AuthenticationException;


    void logout();
}
