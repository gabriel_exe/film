package com.tap5.hotelbooking.services.searcher2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Search implements ISearcher {
    private List<Data> dataList;
    @Override
    public void refresh(String[] classNames, long[] modificationDates) {
        long startRefresh = new Date().getTime();
        int n = classNames.length;
        dataList = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            Data elem = new Data(classNames[i], modificationDates[i]);
            dataList.add(elem);
        }

        Collections.sort(dataList);
        long stopRefresh = new Date().getTime();
        System.out.println(startRefresh);
        System.out.println(stopRefresh);
        System.out.println(stopRefresh - startRefresh);
    }

    @Override
    public String[] guess(String start) {
        long startRefresh = new Date().getTime();
        ArrayList<String> res = new ArrayList<>();
        int count = 0;
        int size = dataList.size();
        for (int i = 0; i < size && count < 12; i++) {
            if (dataList.get(i).getClassName().startsWith(start)){
                res.add(count++,dataList.get(i).getClassName());
            }
        }
        String[] result = new String[res.size()];
        res.toArray(result);
        long stopRefresh = new Date().getTime();
        System.out.println(startRefresh);
        System.out.println(stopRefresh);
        System.out.println(stopRefresh - startRefresh);
        return result;
    }

    private class Data implements Comparable<Data>{
        private String className;
        private long modificationDate;

        private Data() {
        }

        @Override
        public String toString() {
            return modificationDate + " " + className;
        }

        public Data(String className, long modificationDate) {
            this.className = className;
            this.modificationDate = modificationDate;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public long getModificationDate() {
            return modificationDate;
        }

        public void setModificationDate(long modificationDate) {
            this.modificationDate = modificationDate;
        }

        @Override
        public int compareTo(Data o) {
            if (this.getModificationDate() > o.getModificationDate())
                return -1;
            else if (this.getModificationDate() < o.getModificationDate())
                return 1;
            else
                return this.getClassName().compareTo(o.getClassName());
        }
    }
}
