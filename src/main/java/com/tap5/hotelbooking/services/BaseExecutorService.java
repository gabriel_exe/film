package com.tap5.hotelbooking.services;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BaseExecutorService {

	private static ExecutorService executorService;

	private static void executorService(){
		executorService = Executors.newFixedThreadPool(1000);
	}

	public static ExecutorService getInstance(){
		if(executorService == null){
			executorService();
		}

		return executorService;
	}
}
