package com.tap5.hotelbooking.services;

import com.tap5.hotelbooking.db.DAO;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.Session;

import com.tap5.hotelbooking.entities.User;
import com.tap5.hotelbooking.security.AuthenticationException;


public class BasicAuthenticator implements Authenticator
{

    public static final String AUTH_TOKEN = "authToken";


    @Inject
    private Request request;

    public void login(String email, String password) throws AuthenticationException {

        User user = DAO.getUserByEmailAndPassword(email, password);

        if (user == null) {
			throw new AuthenticationException("The user doesn't exist");
		}

        request.getSession(true).setAttribute(AUTH_TOKEN, user);
    }

    public boolean isLoggedIn() {
        Session session = request.getSession(true);

        if (session != null) {
			return session.getAttribute(AUTH_TOKEN) != null;
		}
        return false;
    }

    public void logout() {

        Session session = request.getSession(true);

        if (session != null) {
            session.setAttribute(AUTH_TOKEN, null);
            session.invalidate();
        }
    }

    public User getLoggedUser() {
		User user = isLoggedIn() ? (User) request.getSession(true).getAttribute(AUTH_TOKEN) : null;
		return user;
    }

}
