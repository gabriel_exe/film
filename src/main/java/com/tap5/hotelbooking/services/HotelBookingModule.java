package com.tap5.hotelbooking.services;


import com.tap5.hotelbooking.security.AuthenticationFilter;
import com.tap5.hotelbooking.services.proxy.ProxyHelper;
import com.tap5.hotelbooking.services.proxy.ProxyStorage;
import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.ioc.annotations.Contribute;
import org.apache.tapestry5.ioc.annotations.Startup;
import org.apache.tapestry5.ioc.services.ApplicationDefaults;
import org.apache.tapestry5.ioc.services.SymbolProvider;
import org.apache.tapestry5.services.ComponentRequestFilter;
import org.apache.tapestry5.services.ComponentRequestHandler;
import org.apache.tapestry5.validator.ValidatorMacro;


public class HotelBookingModule
{
    public static void bind(ServiceBinder binder)
    {
        binder.bind(Authenticator.class, BasicAuthenticator.class);
    }

    @ApplicationDefaults
    @Contribute(SymbolProvider.class)
    public static void configureTapestryHotelBooking( MappedConfiguration<String, String> configuration) {

        configuration.add(SymbolConstants.SUPPORTED_LOCALES, "en");
        configuration.add(SymbolConstants.APPLICATION_VERSION, "1.2-SNAPSHOT");
        configuration.add(SymbolConstants.JAVASCRIPT_INFRASTRUCTURE_PROVIDER, "jquery");
		configuration.add(SymbolConstants.BOOTSTRAP_ROOT, "classpath:/META-INF/assets");
        /*configuration.add(UploadSymbols.REPOSITORY_THRESHOLD, "1000000");*/
		/*configuration.add(JQuerySymbolConstants.SUPPRESS_PROTOTYPE, "true");
		configuration.add(JQuerySymbolConstants.JQUERY_ALIAS, "$j");*/
    }

    @Contribute(ValidatorMacro.class)
    public static void combineValidators(MappedConfiguration<String, String> configuration)
    {
        configuration.add("username", "required, minlength=3, maxlength=15");
        configuration.add("password", "required, minlength=6, maxlength=12");
    }

    @Contribute(ComponentRequestHandler.class)
    public static void contributeComponentRequestHandler(
            OrderedConfiguration<ComponentRequestFilter> configuration)
    {
        configuration.addInstance("RequiresLogin", AuthenticationFilter.class);
    }

    @Startup
	public static void initProxyStorage(){
		System.out.println("initProxy");
		ProxyHelper.getInstance();
		ProxyStorage.getInstance();
	}
}
