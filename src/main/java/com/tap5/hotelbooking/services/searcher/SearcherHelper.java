package com.tap5.hotelbooking.services.searcher;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class SearcherHelper implements ISearcher{

	private List<IndexedFile> dataList;

	private String[] names;

	private long[] dates;



	public SearcherHelper() {}



	private class IndexedFile implements Comparable<IndexedFile>{

		private String className;

		private long modificationDate;



		public IndexedFile(String className, long modificationDate) {
			this.className = className;
			this.modificationDate = modificationDate;
		}



		public String getClassName() {
			return className;
		}

		public void setClassName(String className) {
			this.className = className;
		}

		public long getModificationDate() {
			return modificationDate;
		}

		public void setModificationDate(long modificationDate) {
			this.modificationDate = modificationDate;
		}

		@Override
		public int compareTo(IndexedFile o) {
			if (this.getModificationDate() > o.getModificationDate())
				return -1;
			else if (this.getModificationDate() < o.getModificationDate())
				return 1;
			else
				return this.getClassName().compareTo(o.getClassName());
		}
	}

	public List<IndexedFile> getDataList() {
		return dataList;
	}

	public void setDataList(List<IndexedFile> dataList) {
		this.dataList = dataList;
	}

	public String[] getNames() {
		return names;
	}

	public void setNames(String[] names) {
		this.names = names;
	}

	public long[] getDates() {
		return dates;
	}

	public void setDates(long[] dates) {
		this.dates = dates;
	}

	public void readFiles(String dirPath) throws IOException{

		TreeMap<String, Long> files = new TreeMap<>();
		IOFileFilter filter = new SuffixFileFilter(new String[]{"png", "jpg", "exe", "jpeg", "txt", "setup", "jpeg", "gif"}, IOCase.INSENSITIVE);
		Iterator iter = FileUtils.iterateFiles(new File(dirPath), filter, DirectoryFileFilter.DIRECTORY);

        int i = 0;
		while (iter.hasNext()){
			File file = (File) iter.next();
			files.put(file.getName(), file.lastModified());
        }

        this.names = files.keySet().toArray(new String[files.size()]);
        this.dates = ArrayUtils.toPrimitive(files.values().toArray(new Long[files.size()]));
	}

	@Override
	public void refresh(String[] classNames, long[] modificationDates) {

		long startTime = new Date().getTime();

		int n = classNames.length;
		dataList = new ArrayList<>(n);
		for (int i = 0; i < n; i++) {
			IndexedFile elem = new IndexedFile(classNames[i], modificationDates[i]);
			dataList.add(elem);
		}

		Collections.sort(dataList);

		long stopTime = new Date().getTime();
		System.out.println(startTime);
		System.out.println(stopTime);
		System.out.println("refresh-" + String.valueOf(stopTime - startTime));
	}

	@Override
	public String[] guess(String start) {

		long startTime = new Date().getTime();

		ArrayList<String> res = new ArrayList<>();

		int count = 0;
		int size = dataList.size();
		for (int i = 0; i < size && count < 12; i++) {
			if (dataList.get(i).getClassName().startsWith(start)){
				res.add(count++, dataList.get(i).getClassName());
			}
		}

		long stopTime = new Date().getTime();
		System.out.println(startTime);
		System.out.println(stopTime);
		System.out.println("guess-" + String.valueOf(stopTime - startTime));

		return res.toArray(new String[res.size()]);
	}
}
