package com.tap5.hotelbooking.services.searcher;

import java.util.*;

public class Searcher implements ISearcher{

	private List<IndexedFile> indexedFilesList;



	public Searcher() {}



	private class IndexedFile implements Comparable<IndexedFile>{

		private String className;

		private long modificationDate;



		public IndexedFile(String className, long modificationDate) {
			this.className = className;
			this.modificationDate = modificationDate;
		}



		public String getClassName() {
			return className;
		}

		public void setClassName(String className) {
			this.className = className;
		}

		public long getModificationDate() {
			return modificationDate;
		}

		public void setModificationDate(long modificationDate) {
			this.modificationDate = modificationDate;
		}

		@Override
		public int compareTo(IndexedFile o) {
			if (this.getModificationDate() > o.getModificationDate())
				return -1;
			else if (this.getModificationDate() < o.getModificationDate())
				return 1;
			else
				return this.getClassName().compareTo(o.getClassName());
		}
	}

	public List<IndexedFile> getIndexedFilesList() {
		return indexedFilesList;
	}

	public void setIndexedFilesList(List<IndexedFile> indexedFilesList) {
		this.indexedFilesList = indexedFilesList;
	}

	@Override
	public void refresh(String[] classNames, long[] modificationDates) {

		int n = classNames.length;
		indexedFilesList = new ArrayList<>(n);
		for (int i = 0; i < n; i++) {
			IndexedFile elem = new IndexedFile(classNames[i], modificationDates[i]);
			indexedFilesList.add(elem);
		}

		Collections.sort(indexedFilesList);
	}

	@Override
	public String[] guess(String start) {

		ArrayList<String> res = new ArrayList<>();

		int count = 0;
		int size = indexedFilesList.size();
		for (int i = 0; i < size && count < 12; i++) {
			if (indexedFilesList.get(i).getClassName().startsWith(start)){
				res.add(count++, indexedFilesList.get(i).getClassName());
			}
		}

		return res.toArray(new String[res.size()]);
	}
}
