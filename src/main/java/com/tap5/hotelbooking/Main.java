package com.tap5.hotelbooking;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Main {
	public static void main(String[] args){

		String action = args[0];
		boolean zip = (args[1].equals("-zip"));
		String sourceName = zip ? args[2] : args[1];
		String destinationName = zip ? args[3] : args[2];

		File inFile = new File(sourceName);
		File outFile = new File(destinationName);

		try{
			FileInputStream in = new FileInputStream(inFile);
			FileOutputStream out = new FileOutputStream(outFile);

			ZipOutputStream zOut = new ZipOutputStream(new FileOutputStream(outFile));
			if(zip){
				zOut.putNextEntry(new ZipEntry(inFile.getName()));
			}


			if(action.equals("-mv") && !zip){

				Files.move(
					Paths.get(sourceName),
					Paths.get(destinationName),
					StandardCopyOption.REPLACE_EXISTING
				);

				System.out.println("File moved");

			} else {

				long length = in.available();
				long counter = 0;
				int bytes;
				byte[] buffer = new byte[1024];
				while ((bytes = in.read(buffer)) > 0){

					if(!zip){
						out.write(buffer, 0, bytes);
					} else {
						zOut.write(buffer, 0, bytes);
					}

					System.out.write(("\rProgress: " + (int)(((double)(counter+=bytes)/(double)length)*100) + " %").getBytes());
				}

				in.close();
				out.close();
				zOut.closeEntry();
				zOut.close();

				String result = (action.equals("-mv"))
					? "\rFile moved with zip"
					: "\rFile copied" + ((zip) ? " with zip" : "");

				System.out.write(result.getBytes());
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}