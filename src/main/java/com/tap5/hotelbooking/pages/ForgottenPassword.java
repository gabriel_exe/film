package com.tap5.hotelbooking.pages;


import com.tap5.hotelbooking.annotations.AnonymousAccess;
import com.tap5.hotelbooking.db.DAO;
import com.tap5.hotelbooking.entities.User;
import com.tap5.hotelbooking.security.AuthenticationException;
import com.tap5.hotelbooking.services.Authenticator;
import com.tap5.hotelbooking.services.mail.Sender;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.annotations.Inject;

import java.util.Random;

@AnonymousAccess
public class ForgottenPassword {

	@Inject
	ComponentResources resources;

	@Inject
	Authenticator authenticator;

	@Component(id="forgottenZone")
	private Zone forgottenZone;

	@Component(id = "forgottenForm")
	private Form forgottenForm;

	@Component(id = "emailField", parameters = {"clientId=emailField"})
	private TextField emailField;


	@Property
	private boolean success;

	@Property
	@Persist
	private String email;





	Object onValidateFromForgottenForm(){
		if(!forgottenForm.getHasErrors()){

			User user = DAO.getUserByEmail(email);

			if(user == null){
				forgottenForm.recordError(emailField, "Данный e-mail не найден");
			} else {

				String password = "";
				Random random = new Random();
				String[] letters = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};

				for (int i = 0; i < 10; i++) {
					password += letters[random.nextInt(10)];
				}

				String theme = "Восстановление пароля на сервисе v-apelsinah.ru";
				String text =
					"<html>"
						+ "<head></head>"
						+ "<body>"
							+ "Здравствуйте " + user.getUsername()
							+ "<br/>Вы запросили восстановление пароля на сервисе v-apelsinah.ru"
							+ "<br/>Ваш новый пароль: <b>" + password + "</b>"
							+ "<br/>Используйте его для входа на сайт. При желании, Вы можете изменить его в личном кабинете."
						+ "</body>"
					+ "</html>";
				String from = "v.apelsinah.smtp@gmail.com";
				String to = user.getEmail();

				Sender sender = new Sender("v.apelsinah.smtp@gmail.com", "Smolensk863!");
				sender.send(theme, text, from, to);

				user.setPassword(password);
				DAO.update(user);

				success = true;
			}
		}


		return forgottenZone.getBody();
	}
}
