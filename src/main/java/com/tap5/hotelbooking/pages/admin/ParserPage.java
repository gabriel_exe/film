package com.tap5.hotelbooking.pages.admin;


import com.tap5.hotelbooking.annotations.AnonymousAccess;
import com.tap5.hotelbooking.db.DAO;
import com.tap5.hotelbooking.entities.data.Film;
import com.tap5.hotelbooking.services.BaseExecutorService;
import com.tap5.hotelbooking.services.parser.Parser;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@AnonymousAccess
public class ParserPage {


	@Property
	private String filmNumbers;

	@Property
	private int threadCount;

	@Property
	private boolean saveToBd;



	@Component(id = "parserForm")
	private Form parserForm;

	@Component(id = "filmNumbersField")
	private TextField filmNumbersField;

	@Component(id = "threadCountField")
	private TextField threadCountField;

	@Component(id = "saveToBdCheckbox")
	private Checkbox saveToBdCheckbox;


	void onValidateFromParserForm(){

		threadCount = threadCount == 0 ? 20 : threadCount;

		String[] numbers = filmNumbers.split("-");
		int from = Integer.parseInt(numbers[0]);
		int to = Integer.parseInt(numbers[1]);
		int itStop = (to - from) / threadCount;

		System.out.println(threadCount);

		List<Film> films = new ArrayList<>();
		Set<Future<Film>> futures = new HashSet<>();

		for(int i = 0; i <= itStop; i++){
			int st = from + (threadCount * i);
			int sto = from + (threadCount * (i + 1)) - 1;

			for(int j = st; j <= sto && j <= to; j++){
				futures.add(BaseExecutorService.getInstance().submit(new Parser(j)));
			}

			for(Future<Film> future : futures){
				try {

					Film film = future.get();
					films.add(film);

					if(saveToBd){
						DAO.saveOrUpdate(film);
					}

				} catch (InterruptedException | ExecutionException e){
					e.printStackTrace();
				}
			}

			futures.clear();
		}
	}
}
