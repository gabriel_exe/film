package com.tap5.hotelbooking.pages.admin;

import com.tap5.hotelbooking.annotations.AnonymousAccess;
import com.tap5.hotelbooking.db.DAO;
import com.tap5.hotelbooking.entities.data.Country;
import com.tap5.hotelbooking.entities.data.Genre;
import com.tap5.hotelbooking.entities.game.Rules;
import com.tap5.hotelbooking.services.CountryEncoder;
import com.tap5.hotelbooking.services.GenreEncoder;
import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.*;
import org.apache.tapestry5.internal.services.StringValueEncoder;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.SelectModelFactory;

import java.util.ArrayList;
import java.util.List;

@AnonymousAccess
public class RulesPage {

	@Property
	private final StringValueEncoder stringValueEncoder = new StringValueEncoder();

	@Inject
	private SelectModelFactory selectModelFactory;

	@Component(id = "rulesListZone")
	private Zone rulesListZone;

	@Property
	private List<Rules> rulesList;

	@Property
	private Rules rules = new Rules();

	@Property
	private Genre genre;



	@Component(id = "rulesFormZone")
	private Zone rulesFormZone;

	@Component(id = "rulesForm")
	private Form rulesForm;


	@Property
	private long id;

	@Component(id = "idField")
	private Hidden idField;

	@Property
	private String name;

	@Component(id = "nameField")
	private TextField nameField;

	@Property
	private String image;

	@Component(id = "imageField")
	private TextField imageField;

	@Property
	@Persist
	private int minYear;

	@Property
	@Persist
	private int maxYear;

	@Property
	private int fromYear;

	@Property
	private int toYear;

	@Property
	private boolean allowedFilms;

	@Property
	private boolean allowedSerials;

	@Property
	private boolean allowedMiniSerials;

	@Property
	private GenreEncoder genreEncoder = new GenreEncoder();

	@Property
	@Persist
	private List<Genre> genres;

	@Property
	@Persist
	private SelectModel genreSelectModel;

	@Property
	private List<Genre> selectedGenres = new ArrayList<>();

	@Property
	private CountryEncoder countryEncoder = new CountryEncoder();

	@Property
	@Persist
	private List<Country> countries;

	@Property
	@Persist
	private SelectModel countriesSelectModel;

	@Property
	private List<Country> selectedCountries = new ArrayList<>();




	void setupRender(){

		int minYear = DAO.getYear("min");
		int maxYear = DAO.getYear("max");
		List<Genre> genresList = DAO.getList(Genre.class);
		List<Country> countriesList = DAO.getList(Country.class);

		rulesList = DAO.getList(Rules.class);





		this.minYear = minYear;
		this.maxYear = maxYear;
		this.fromYear = minYear;
		this.toYear = maxYear;




		this.genres = genresList;
		this.genreSelectModel = selectModelFactory.create(genresList, "name");
		this.countries = countriesList;
		this.countriesSelectModel = selectModelFactory.create(countriesList, "name");

	}

	Object onValidateFromRulesForm(){

		rules.setGenres(selectedGenres);
		rules.setCountries(selectedCountries);

		DAO.saveOrUpdate(rules);


		rulesList = DAO.getList(Rules.class);

		return rulesListZone.getBody();
	}


	@OnEvent(value = "edit")
	Object edit(int id){
		this.rules = DAO.getById(Rules.class, id);

		this.selectedGenres.clear();
		for(Genre genre : this.genres){
			if(rules.getGenres().contains(genre)){
				this.selectedGenres.add(genre);
			}
		}
		this.selectedCountries.clear();
		for(Country country : this.countries){
			if(rules.getCountries().contains(country)){
				this.selectedCountries.add(country);
			}
		}

		return rulesFormZone.getBody();
	}

	@OnEvent(value = "remove")
	Object remove(int id){
		DAO.deleteById(Rules.class, id);
		rulesList = DAO.getList(Rules.class);
		return rulesListZone.getBody();
	}



}
