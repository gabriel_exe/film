package com.tap5.hotelbooking.pages.cabinet;


import com.tap5.hotelbooking.db.DAO;
import com.tap5.hotelbooking.entities.User;
import com.tap5.hotelbooking.services.Authenticator;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.annotations.Inject;

import javax.jws.soap.SOAPBinding;

public class Data {

	@Inject
	private Authenticator authenticator;

	@Property
	private boolean success;

	@Property
	@Persist
	private User user;

	@Property
	@Persist
	private String email;



	@Component(id = "dataZone")
	private Zone dataZone;

	@Component(id = "dataForm")
	private Form dataForm;

	@Component(id = "emailField", parameters = {"clientId=emailField"})
	private TextField emailField;



	void setupRender(){
		user = authenticator.getLoggedUser();
		email = user.getEmail();
	}

	Object onValidateFromDataForm(){

		user = authenticator.getLoggedUser();

		if (!dataForm.getHasErrors() && !email.equals(user.getEmail())){

			User checkUser = DAO.getUserByEmail(email);
			if (checkUser != null){
				dataForm.recordError(emailField, "Адрес уже используется");
			} else {
				user.setEmail(email);
				DAO.update(user);
				success = true;
			}
		}

		return dataZone.getBody();
	}







}
