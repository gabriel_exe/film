package com.tap5.hotelbooking.pages.cabinet;



import com.tap5.hotelbooking.db.DAO;
import com.tap5.hotelbooking.entities.User;
import com.tap5.hotelbooking.services.Authenticator;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.xalan.xsltc.compiler.util.VoidType;

public class Password {

	@Inject
	private Authenticator authenticator;

	@Property
	private User user;

	@Property
	private boolean successMessage;

	@Property
	private String oldPassword;

	@Property
	private String newPassword;

	@Property
	private String confirmPassword;


	@Component(id = "passwordZone")
	private Zone passwordZone;

	@Component(id = "passwordForm")
	private Form passwordForm;

	@Component(id = "oldPasswordField", parameters = {"clientId=oldPasswordField"})
	private PasswordField oldPasswordField;

	@Component(id = "newPasswordField", parameters = {"clientId=newPasswordField"})
	private PasswordField newPasswordField;

	@Component(id = "confirmPasswordField", parameters = {"clientId=confirmPasswordField"})
	private PasswordField confirmPasswordField;


	Object onValidateFromPasswordForm(){

		if(!passwordForm.getHasErrors()){
			user = authenticator.getLoggedUser();

			if(oldPassword.equals(user.getPassword())){
				if(!newPassword.equals(confirmPassword)){
					passwordForm.recordError(confirmPasswordField, "Пароли не совпадают");
				} else {
					user.setPassword(newPassword);
					DAO.update(user);
					successMessage = true;
				}
			} else {
				passwordForm.recordError(oldPasswordField, "Неверный пароль");
			}
		}

		return passwordZone.getBody();
	}
}
