package com.tap5.hotelbooking.pages;

import com.tap5.hotelbooking.annotations.AnonymousAccess;
import com.tap5.hotelbooking.db.DAO;
import com.tap5.hotelbooking.entities.User;
import com.tap5.hotelbooking.entities.game.Game;
import com.tap5.hotelbooking.entities.game.Rules;
import com.tap5.hotelbooking.services.Authenticator;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.annotations.Inject;

import java.util.List;

@AnonymousAccess
public class Rating {

	@Inject
	private Authenticator authenticator;

	@Component
	Zone topZone;

	@Property
	private String period;

	@Property
	int listIndex;

	@Property
	private List<Game> gamesList;

	@Property
	private List<Rules> rulesList;

	@Property
	private Game game;

	void setupRender(){
		this.period = "today";
		this.gamesList = DAO.gelGamesList("today");
	}

	Object onUpdateList(String period){
		this.period = period;
		this.gamesList = DAO.gelGamesList(period);
		return topZone.getBody();
	}

	public User getUser(){
		return authenticator.getLoggedUser();
	}

	public int getPositionIndex(int index){
		return index + 1;
	}
}
