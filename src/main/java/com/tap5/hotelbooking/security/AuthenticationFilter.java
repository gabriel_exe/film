package com.tap5.hotelbooking.security;

import com.tap5.hotelbooking.annotations.AnonymousAccess;
import com.tap5.hotelbooking.pages.Game;
import com.tap5.hotelbooking.services.Authenticator;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.runtime.Component;
import org.apache.tapestry5.services.*;

import java.io.IOException;

public class AuthenticationFilter implements ComponentRequestFilter
{

    private final PageRenderLinkSource renderLinkSource;

    private final ComponentSource componentSource;

    private final Response response;

    private final Authenticator authenticator;

    private String defaultPage = Game.class.getSimpleName();

    private String startPage = Game.class.getSimpleName();

    public AuthenticationFilter(PageRenderLinkSource renderLinkSource, ComponentSource componentSource, Response response, Authenticator authenticator)
    {
        this.renderLinkSource = renderLinkSource;
        this.componentSource = componentSource;
        this.response = response;
        this.authenticator = authenticator;
    }

    public void handleComponentEvent(ComponentEventRequestParameters parameters, ComponentRequestHandler handler) throws IOException
    {

        if (dispatchedToLoginPage(parameters.getActivePageName())) { return; }

        handler.handleComponentEvent(parameters);

    }

    public void handlePageRender(PageRenderRequestParameters parameters, ComponentRequestHandler handler) throws IOException
    {

        if (dispatchedToLoginPage(parameters.getLogicalPageName())) { return; }

        handler.handlePageRender(parameters);
    }

	// Мето проверяет нужно ли отправить пользователя на страницу входа,
	// Нужно, если он не авторизован,
	// Не нужно если авторизован
    private boolean dispatchedToLoginPage(String pageName) throws IOException {


		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setDateHeader("Expires", 0);

		// Авторизованный пользователь, не направляется на страницу логина (return false).
        if (authenticator.isLoggedIn()){

			// Авторизованный пользователь, который идет на страницу логина,
			// должен не должен попасть на неё,
			// вмtcто этого отправляем его на страницу обзора.
            /*if (startPage.equalsIgnoreCase(pageName)){
                Link link = renderLinkSource.createPageRenderLink(defaultPage);
                response.sendRedirect(link);
                return true;
            }*/

            return false;
        } else {
			if(pageName.equals("cabinet/Data") || pageName.equals("cabinet/Password")) {
				Link link = renderLinkSource.createPageRenderLink("enter");
				response.sendRedirect(link);
				return true;
			};
		}

        Component page = componentSource.getPage(pageName);

        if (page.getClass().isAnnotationPresent(AnonymousAccess.class)) { return false; }

        Link link = renderLinkSource.createPageRenderLink(pageName);

        response.sendRedirect(link);

        return true;
    }
}
