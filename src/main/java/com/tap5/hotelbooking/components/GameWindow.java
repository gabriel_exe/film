package com.tap5.hotelbooking.components;

import com.tap5.hotelbooking.db.DAO;
import com.tap5.hotelbooking.entities.User;
import com.tap5.hotelbooking.entities.data.Country;
import com.tap5.hotelbooking.entities.data.Genre;
import com.tap5.hotelbooking.entities.game.Game;
import com.tap5.hotelbooking.entities.game.Rules;
import com.tap5.hotelbooking.entities.game.Question;
import com.tap5.hotelbooking.entities.game.Variant;
import com.tap5.hotelbooking.services.Authenticator;
import com.tap5.hotelbooking.services.CountryEncoder;
import com.tap5.hotelbooking.services.GameService;
import com.tap5.hotelbooking.services.GenreEncoder;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.*;
import org.apache.tapestry5.corelib.components.*;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Cookies;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.SelectModelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameWindow {

    final static Logger logger = LoggerFactory.getLogger(GameWindow.class);

	/*
	* Поля
	* */

	@Property
	private String rulesType = getSelectedRulesType();

	@Property
	private int systemRulesId = getSelectedRulesId();

	@Property
	private boolean badRules = false;

	@Property
	private Game game;

	@Property
	private Variant variant;

	@Property
	private boolean pause = false;

	@Property
	private Rules rules;





	/*
	* Форма для правил
	* */

	@Component(id = "rulesForm")
	private Form form;

	@Component(id = "rulesType")
	private Hidden rulesTypeField;

	@Component(id = "systemRulesId")
	private Hidden systemRulesIdField;



    @Inject
    private Request request;

    @Inject
    private Cookies cookies;

	@Inject
	Authenticator authenticator;

    @InjectComponent
    private Zone gameZone;

    @InjectComponent
    private EventLink startNewGameLink;

    @Component(id = "timeIsOverLink")
    private EventLink timeIsOverLink;

	@Component(id = "resumeLink")
	private EventLink resumeLink;

	@Component(id = "completeLink")
	private EventLink completeLink;


    void setupRender(){

        Game game = getActiveGame();

		if (game != null && !game.getStatus().equals("paused") && game.isAnswerTimeHasPassed()){
            game.complete("timeIsOver");
			DAO.update(game);
        }

        this.game = game;
    }

    private Game getActiveGame(){
        String playerType = (authenticator.isLoggedIn()) ? "authorized" : "unauthorized";
        String sessionId = cookies.readCookieValue("JSESSIONID");
        long userId = (authenticator.isLoggedIn()) ? authenticator.getLoggedUser().getId() : 0;

        return DAO.getActiveGame(playerType, sessionId, userId);
    }

	public List<Rules> getSystemRules(){
		return DAO.getSystemRulesList();
	}

	public String getSelectedRulesType(){
		return (rulesType == null) ? "system" : rulesType;
	}

	public int getSelectedRulesId(){
		return (rulesType.equals("system") && systemRulesId == 0) ? 1 : systemRulesId;
	}

	Object onSuccess(){

		String playerType = (authenticator.isLoggedIn()) ? "authorized" : "unauthorized";
		User user = (authenticator.isLoggedIn()) ? authenticator.getLoggedUser() : null;


		Rules selectedRules = DAO.getById(Rules.class, systemRulesId);

		if(selectedRules.checkFilmsCountOnThisRules()){
			Game newGame = new Game(playerType, cookies.readCookieValue("JSESSIONID"), user);

			Question question = GameService.createRandomQuestion(selectedRules);

			newGame.addCurrentQuestion(question);
			newGame.addRules(selectedRules);

			DAO.save(newGame);

			game = newGame;
		} else {
			this.badRules = true;
		}

		return request.isXHR() ? gameZone.getBody() : null;
	}

    @OnEvent(value = "answer")
    Object onAnswer(int answerNumber, int pause){

        Game game = getActiveGame();

        if(game.getCurrentQuestion().getAnswerNumber() == answerNumber){

            game.setAnswersCount(game.getAnswersCount() + 1);
            game.actualizeTip();

			if(pause == 1){
				this.pause = true;
				game.setStatus("paused");
			} else {
				Question question = GameService.createRandomQuestion(game.getRules());
				game.addCurrentQuestion(question);
			}
        }
        else{
            game.complete("incorrectAnswer");
        }

        DAO.update(game);
        this.game = game;
        return request.isXHR() ? gameZone.getBody() : null;
    }

    @OnEvent(value = "tip")
    Object onTip(String tip){

        Game game = getActiveGame();

        if (tip.equals("skip") && game.isTipSkip()){

            game.addCurrentQuestion(GameService.createRandomQuestion(game.getRules()));
            game.setTipSkip(false);

            game.actualizeTip();

        }
        else if (tip.equals("fiftyFifty") && game.getTipFiftyFifty().equals("available")){

            int[] arr = new int[]{1,2,3,4};
            int[] arrWithoutAnswer = ArrayUtils.remove(arr, game.getCurrentQuestion().getAnswerNumber() - 1);
            int r = new Random().nextInt(arrWithoutAnswer.length);

            game.setTipFiftyFifty("active");
            game.setSecondVariantForFiftyFifty(arrWithoutAnswer[r]);

        }
        else if(tip.equals("additionalImage") && game.getTipAdditionalImage().equals("available")){
            game.setTipAdditionalImage("active");
        }

        this.game = game;
        DAO.update(game);


        return request.isXHR() ? gameZone.getBody() : null;
    }

	@OnEvent(value = "complete")
	Object onComplete(){
		Game game = getActiveGame();
		game.complete("userSolution");

		DAO.update(game);
		this.game = game;
		return request.isXHR() ? gameZone.getBody() : null;
	}

	@OnEvent(value = "resume")
	Object onResume(){
        Game game = getActiveGame();

        Question question = GameService.createRandomQuestion(game.getRules());
        game.addCurrentQuestion(question);
        game.setStatus("active");

        DAO.update(game);
        this.game = game;
        return request.isXHR() ? gameZone.getBody() : null;
	}

    @OnEvent(value = "timeIsOver")
    Object onTimeIsOver(){

        Game game = getActiveGame();
        game.complete("timeIsOver");
        DAO.update(game);

        this.game = game;

        return request.isXHR() ? gameZone.getBody() : null;
    }

	@OnEvent(value = "startNewGame")
	Object onStartNewGame(){
		this.game = null;
		return request.isXHR() ? gameZone.getBody() : null;
	}
}
