package com.tap5.hotelbooking.components;

import com.tap5.hotelbooking.pages.Game;
import com.tap5.hotelbooking.security.AuthenticationException;
import com.tap5.hotelbooking.services.Authenticator;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PageRenderLinkSource;


public class EnterForm {

	@Inject
	private PageRenderLinkSource pageRenderLinkSource;

	@Inject
	ComponentResources resources;

	@Inject
	Authenticator authenticator;

	@Component(id="enterZone")
	Zone enterZone;

	@Component(id = "enterForm")
	private Form enterForm;

	@Component(id = "emailField", parameters = {"clientId=emailField"})
	private TextField emailField;

	@Component(id = "passwordField", parameters = {"clientId=passwordField"})
	private PasswordField passwordField;



	@Property
	@Persist
	private String email;

	@Property
	@Persist
	private String password;




	Object onValidateFromEnterForm(){
		try {
			authenticator.login(email, password);
			return pageRenderLinkSource.createPageRenderLinkWithContext(Game.class);
		} catch (AuthenticationException ex) {
			enterForm.recordError(emailField, "Не верная пара логин пароль");
		}

		return enterZone.getBody();
	}
}
