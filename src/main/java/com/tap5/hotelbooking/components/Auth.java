package com.tap5.hotelbooking.components;

import com.tap5.hotelbooking.entities.User;
import com.tap5.hotelbooking.services.Authenticator;
import org.apache.tapestry5.ioc.annotations.Inject;

public class Auth{

	@Inject
	private Authenticator authenticator;

	public User getUser(){
		return authenticator.getLoggedUser();
	}

}
