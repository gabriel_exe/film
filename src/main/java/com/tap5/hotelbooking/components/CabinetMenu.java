package com.tap5.hotelbooking.components;

import com.tap5.hotelbooking.pages.Game;
import com.tap5.hotelbooking.services.Authenticator;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PageRenderLinkSource;

public class CabinetMenu {

	@Inject
	private PageRenderLinkSource pageRenderLinkSource;

	@Inject
	private Authenticator authenticator;

	public Object onActionFromLogout(){
		authenticator.logout();
		return pageRenderLinkSource.createPageRenderLinkWithContext(Game.class);
	}
}
