package com.tap5.hotelbooking.components;

import com.tap5.hotelbooking.db.DAO;
import com.tap5.hotelbooking.entities.User;
import com.tap5.hotelbooking.pages.Registration;
import com.tap5.hotelbooking.pages.Game;
import com.tap5.hotelbooking.services.Authenticator;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.PersistenceConstants;
import org.apache.tapestry5.annotations.*;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PageRenderLinkSource;

public class RegistrationForm {

	@Inject
	private PageRenderLinkSource pageRenderLinkSource;

	@InjectPage
	Registration registrationPage;

	@Inject
	Authenticator authenticator;

	@Inject
	ComponentResources componentResources;

	@Component
	private Zone registrationZone;

	@Component(id="registrationForm")
	private Form registrationForm;

	@Component(id="usernameField", parameters = {"clientId=usernameField"})
	private TextField usernameField;

	@Component(id = "emailField", parameters = {"clientId=emailField"})
	private TextField emailField;

	@Component(id = "passwordField", parameters = {"clientId=passwordField"})
	private PasswordField passwordField;



	@Property
	@Persist(value = PersistenceConstants.FLASH)
	private String username;

	@Property
	@Persist(value = PersistenceConstants.FLASH)
	private String email;

	@Property
	private String password;




	Object onValidateFromRegistrationForm(){

		boolean usernameValid = false;
		boolean emailValid = false;
		boolean passwordValid = false;

		if(username == null || username.equals("")){
			registrationForm.recordError(usernameField, "Укажите логин");
		} else if(username.length() < 3 || username.length() > 20){
			registrationForm.recordError(usernameField, "Длинна логина от 3 до 20 символов");
		} else if(!username.matches("^[A-zА-я0-9-_.]*$")){
			registrationForm.recordError(usernameField, "Логин содержит недопустимые символы");
		} else if(!DAO.checkUsername(username)){
			registrationForm.recordError(usernameField, "Логин уже используется, выберите другой");
		} else {
			usernameValid = true;
		}

		if(email == null || email.equals("")){
			registrationForm.recordError(emailField, "Укажите почтовый адресс");
		} else if(!email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")){
			registrationForm.recordError(emailField, "Некорректный почтовый адресс");
		} else if(email.length() > 50){
			registrationForm.recordError(emailField, "Максимальная длинна почтового адреса 50 символов");
		} else if(!DAO.checkEmail(email)){
			registrationForm.recordError(emailField, "Почтовый ящик уже используется, выберите другой");
		} else {
			emailValid = true;
		}

		if(password == null || password.equals("")){
			registrationForm.recordError(passwordField, "Укажите пароль");
		} else if(password.length() < 6 || password.length() > 20){
			registrationForm.recordError(passwordField, "Длинна пароля от 6 до 20 символов");
		} else {
			passwordValid = true;
		}

		if(usernameValid && emailValid && passwordValid){
			User user = new User(username, email, password);
			try {
				DAO.save(user);
				componentResources.discardPersistentFieldChanges();
				authenticator.login(email, password);
				return pageRenderLinkSource.createPageRenderLinkWithContext(Game.class);
			} catch (Exception e){
				return Registration.class;
			}
		}

		return registrationZone.getBody();
	}
}
