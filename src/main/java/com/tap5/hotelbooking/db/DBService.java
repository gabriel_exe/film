package com.tap5.hotelbooking.db;

import com.tap5.hotelbooking.entities.data.*;
import com.tap5.hotelbooking.entities.User;
import com.tap5.hotelbooking.entities.game.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;


public class DBService {
    private static DBService dbService;

    private static SessionFactory sessionFactory;

    private DBService() {
        Configuration configuration = getConfiguration();
        sessionFactory = getSessionFactory(configuration);
    }

    public static DBService getInstance(){

        if(dbService == null){
            dbService = new DBService();
        }

        return dbService;
    }

    public static SessionFactory getSessionFactory(Configuration configuration){
        StandardServiceRegistryBuilder standardServiceRegistryBuilder = new StandardServiceRegistryBuilder();
        standardServiceRegistryBuilder.applySettings(configuration.getProperties());
        ServiceRegistry serviceRegistry = standardServiceRegistryBuilder.build();
        return configuration.buildSessionFactory(serviceRegistry);
    }

    public Configuration getConfiguration(){
        Configuration configuration = new Configuration();

        configuration.addAnnotatedClass(Question.class);
        configuration.addAnnotatedClass(Variant.class);
        configuration.addAnnotatedClass(User.class);
        configuration.addAnnotatedClass(Game.class);
        configuration.addAnnotatedClass(Film.class);
        configuration.addAnnotatedClass(Country.class);
        configuration.addAnnotatedClass(Genre.class);
        configuration.addAnnotatedClass(Still.class);
        configuration.addAnnotatedClass(Person.class);
        configuration.addAnnotatedClass(Rules.class);
		configuration.addAnnotatedClass(Tag.class);


        configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        configuration.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
        configuration.setProperty("hibernate.connection.url", "jdbc:mysql://188.120.236.241:3306/film_remote");
        configuration.setProperty("hibernate.connection.username", "admin");
        configuration.setProperty("hibernate.connection.password", "db_admin_NCC-1701");

        /*configuration.setProperty("hibernate.show_sql", "true");
        configuration.setProperty("hibernate.format_sql", "true");
        configuration.setProperty("hibernate.use_sql_comments", "true");*/

        configuration.setProperty("hibernate.connection.CharSet", "utf8");
        configuration.setProperty("hibernate.connection.characterEncoding", "utf8");
        configuration.setProperty("hibernate.connection.useUnicode", "true");

        configuration.setProperty("hibernate.hbm2ddl.auto", "update");
        return configuration;
    }

    public Session getSession(){
        return sessionFactory.openSession();
    }
}
