package com.tap5.hotelbooking.db;

import com.tap5.hotelbooking.entities.User;
import com.tap5.hotelbooking.entities.data.Film;
import com.tap5.hotelbooking.entities.game.Game;
import com.tap5.hotelbooking.entities.game.Rules;
import com.tap5.hotelbooking.entities.game.Question;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.time.Year;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DAO {

	public static <T> long save(T entity){
		Session session = DBService.getInstance().getSession();
		session.getTransaction().begin();
		long id = (long) session.save(entity);
		session.getTransaction().commit();
		session.close();
		return id;
	}

	public static <T> void update(T entity){
		Session session = DBService.getInstance().getSession();
		session.getTransaction().begin();
		session.update(entity);
		session.getTransaction().commit();
		session.close();
	}

	public static <T> void saveOrUpdate(T entity){
		Session session = DBService.getInstance().getSession();
		session.getTransaction().begin();
		session.saveOrUpdate(entity);
		session.getTransaction().commit();
		session.close();
	}

	public static <T> void merge(T entity){
		Session session = DBService.getInstance().getSession();
		session.getTransaction().begin();
		session.merge(entity);
		session.getTransaction().commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	public static <T> T getById(Class<T> entity, long id){

		T result = null;
		Session session = DBService.getInstance().getSession();

		try {
			result = (T) session.get(entity, id);
		} finally {
			if (session != null && session.isOpen()){
				session.close();
			}
		}

		return result;
	}

	public static <T> T getById2(Class<T> entity, long id){

		T result = null;
		Session session = DBService.getInstance().getSession();

		try {
			result = (T) session.get(entity, id);
		} finally {
			/*if (session != null && session.isOpen()){
				session.close();
			}*/
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	public static <T> T getByStringId(Class<T> entity, String id){
		Session session = DBService.getInstance().getSession();
		Criteria criteria = session.createCriteria(entity);
		criteria.setFetchMode("films", FetchMode.JOIN);
		criteria.add(Restrictions.eq("name", id));

		return (T) criteria.uniqueResult();
	}

	public static <T> List<T> getByFields(Class<T> entity, Criterion criterion){
		Session session = DBService.getInstance().getSession();
		Criteria criteria = session.createCriteria(entity);
		criteria.add(criterion);
		return criteria.list();
	}

	public static <T> void delete(T entity){
		Session session = DBService.getInstance().getSession();
		session.getTransaction().begin();
		session.delete(entity);
		session.getTransaction().commit();
		session.close();
	}

	public static boolean checkUsername(String username){
		Session session = DBService.getInstance().getSession();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("username", username));
		return criteria.list().size() == 0;
	}

	public static boolean checkEmail(String email){
		Session session = DBService.getInstance().getSession();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("email", email));
		return criteria.list().size() == 0;
	}

	public static <T> boolean deleteById(Class<?> type, long id){
		Session session = DBService.getInstance().getSession();
		Object persistentInstance = session.load(type, id);
		if (persistentInstance != null) {
			session.beginTransaction();
			session.delete(persistentInstance);
			session.getTransaction().commit();
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public static Game getActiveGame(String playerType, String sessionId, long userId){

		Session session = DBService.getInstance().getSession();
		Game game = null;

		try{
			Criteria criteria = session.createCriteria(Game.class, "game");
			criteria.add(Restrictions.and(
				Restrictions.eq("playerType", playerType),
				Restrictions.in("status", new ArrayList<>(Arrays.asList("active", "paused")))
			));

			if(playerType.equals("authorized")) {
				criteria.createAlias("game.user", "user");
				criteria.add(Restrictions.eq("user.id", userId));
			} else {
				criteria.add(Restrictions.eq("sessionCookie", sessionId));
			}

			game = (Game) criteria.uniqueResult();
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()){
				session.flush();
				session.close();
			}
		}

		return game;
	}

	public static User getUserByEmailAndPassword(String email, String password){
		Session session = DBService.getInstance().getSession();
		Criteria criteria = session.createCriteria(User.class);
		return (User) criteria.add(Restrictions.and(
				Restrictions.eq("email", email),
				Restrictions.eq("password", password)
			)).uniqueResult();
	}

	public static User getUserByEmail(String email){
		Session session = DBService.getInstance().getSession();
		Criteria criteria = session.createCriteria(User.class);
		return (User) criteria.add(Restrictions.and(
			Restrictions.eq("email", email)
		)).uniqueResult();
	}

	public static Question getRandomQuestion(){
		Session session = DBService.getInstance().getSession();
		Criteria criteria = session.createCriteria(Question.class);
		criteria.add(Restrictions.sqlRestriction("1=1 order by rand()"));
		criteria.setMaxResults(1);
		return (Question) criteria.uniqueResult();
	}

	public static List<Film> getRandomFilmForQuestions(Rules rules, boolean check){
		Session session = DBService.getInstance().getSession();
		Criteria criteria = session.createCriteria(Film.class, "film");
		criteria.createAlias("film.genres", "genres");
		criteria.createAlias("film.countries", "countries");

		if(!rules.isAllowedFilms() || !rules.isAllowedSerials() || !rules.isAllowedMiniSerials()){
			criteria.add(Restrictions.in("film.type", rules.getTypes()));
		}

		criteria.add(Restrictions.eq("availableForGame", true));
		if(rules.getMinYear() != 0 && rules.getMaxYear() != 0){
			criteria.add(Restrictions.between("releaseDate", rules.getMinYear(), rules.getMaxYear()));
		}

		if(rules.getGenres() != null && rules.getGenres().size() != 0){
			criteria.add(Restrictions.in("genres.name", rules.getGenresNames()));
		}

		if(rules.getCountries() != null && rules.getCountries().size() != 0){
			criteria.add(Restrictions.in("countries.name", rules.getCountriesNames()));
		}

		criteria.add(Restrictions.sqlRestriction("1=1 order by rand()"));

		criteria.setMaxResults(check ? 100 : 4);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		return criteria.list();
	}

	public static <T> List<T> getList(Class<T> entity){
		Session session = DBService.getInstance().getSession();
		List<T> list = null;

		try {
			list = session.createCriteria(entity).list();
		} finally {
			if (session != null && session.isOpen()){
				//session.close();
			}
		}

		return list;
	}

	public static List<Game> gelGamesList(String period){
		ZonedDateTime now = ZonedDateTime.now();
		ZonedDateTime start;
		ZonedDateTime end;

		switch (period){
			case "today" :
				start = ZonedDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(), 0, 0, 0, 0, ZoneId.systemDefault());
				end = start.plusDays(1).minusSeconds(1);
				break;
			case "week" :
				start = ZonedDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(), 0, 0, 0, 0, ZoneId.systemDefault())
					.minusDays(now.getDayOfWeek().getValue());
				end = start.plusDays(7).minusSeconds(1);
				break;
			case "month" :
				start = ZonedDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(), 0, 0, 0, 0, ZoneId.systemDefault())
					.minusDays(now.getDayOfMonth());
				end = start.plusDays(now.getMonth().length(Year.isLeap(now.getYear()))).minusSeconds(1);
				break;
			default: // текущий год
				start = ZonedDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(), 0, 0, 0, 0, ZoneId.systemDefault())
					.minusDays(now.getDayOfYear());
				end = start.plusDays(Year.of(now.getYear()).length()).minusSeconds(1);
				break;
		}

		long from = start.toInstant().getEpochSecond();
		long to = end.toInstant().getEpochSecond();


		Session session = DBService.getInstance().getSession();
		Criteria criteria = session.createCriteria(Game.class, "game");
		criteria.add(Restrictions.eq("game.playerType", "authorized"));
		criteria.add(Restrictions.eq("game.status", "completed"));
		criteria.add(Restrictions.gt("game.answersCount", 0));
		criteria.add(Restrictions.between("game.endTime", from, to));
		criteria.addOrder(Order.desc("game.answersCount"));
		criteria.addOrder(Order.asc("game.endTime"));
		criteria.setMaxResults(100);

		return criteria.list();
	}

	public static int getYear(String type){

		Order order = (type.equals("min")) ? Order.asc("releaseDate") : Order.desc("releaseDate");

		Session session = DBService.getInstance().getSession();
		Criteria criteria =
			session.createCriteria(Film.class, "film")
				.add(Restrictions.not(Restrictions.eq("film.name", "PAGE_FILM_DOES_NOT_EXIST")))
				.add(Restrictions.isNotNull("releaseDate"))
				.add(Restrictions.not(Restrictions.eq("releaseDate", 0)))
				.addOrder(order)
				.setMaxResults(1);

		Film film = (Film) criteria.uniqueResult();

		return Integer.valueOf(film.getReleaseDate());
	}

	public static List<Rules> getSystemRulesList(){
		Session session = DBService.getInstance().getSession();
		return session.createCriteria(Rules.class, "rules")
			.add(Restrictions.eq("type", "system"))
			.list();
	}
}
